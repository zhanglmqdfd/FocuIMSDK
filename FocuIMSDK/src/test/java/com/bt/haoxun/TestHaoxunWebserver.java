package com.bt.haoxun;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.commons.util.HttpClientUtil;
import cn.com.focu.im.sdk.service.user.entity.UserStateGet;

public class TestHaoxunWebserver {
	 public static void main(String[] args){
		 String str=get();
		 System.out.println(str);
	  }
	 
	// 根据用户名或者userid获取该用户状态
		public static String find() {
			String rs = "";
			try {
				User  user=new User();
				user.setLoginNumber("15814430584");
				JSONObject json = JSONObject.fromObject(user);
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("jsonString", URLEncoder
						.encode(json.toString(), "UTF-8"));
				rs = postReturnString(params);
				
			} catch (IOException e) {
				try {
					throw new Exception(e);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} catch (Exception e) {
			}
			return rs;
		}
	 
	// 根据用户名或者userid获取该用户状态
	public static String get() {
		String rs = "";
		try {
			UserStateGet  user=new UserStateGet();
			user.setLoginName(new String[]{"admin"});
			JSONObject json = JSONObject.fromObject(user);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			
		} catch (IOException e) {
			try {
				throw new Exception(e);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (Exception e) {
		}
		return rs;
	}
	
	/**
	 * Post方式请求，返回String.
	 * 
	 * @param params
	 *            参数集合
	 * @return 返回值
	 * @throws Exception
	 */
	public static String postReturnString(Map<String, Object> params)
			throws ConnectException, IOException {
		String interfaceUrl ="http://localhost:8280/mng/httpservices/ustate-get.action";
		return HttpClientUtil.postReturnString(interfaceUrl, params);
	}
}
