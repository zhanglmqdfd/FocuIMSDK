package com.bt.haoxun;

public class User {
	private String mobile;
	private String loginNumber;
	private String uid;
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public User(String mobile, String uid) {
		super();
		this.mobile = mobile;
		this.uid = uid;
	}
	public User(String mobile) {
		super();
		this.mobile = mobile;
	}
	public User() {
		super();
	}
	public String getLoginNumber() {
		return loginNumber;
	}
	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}
	
}
