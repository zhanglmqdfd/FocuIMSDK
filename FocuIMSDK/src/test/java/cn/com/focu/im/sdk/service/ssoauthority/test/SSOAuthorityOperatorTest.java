package cn.com.focu.im.sdk.service.ssoauthority.test;
import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.sso.SSOAuthorityOperator;
import cn.com.focu.im.sdk.service.sso.entity.SSOAuthirtyLogin;
import cn.com.focu.im.sdk.service.sso.exception.SSOAuthorityException;
public class SSOAuthorityOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	//获得认证
	@Test
	public void testLogin() {
		try {
			SSOAuthirtyLogin sal = new SSOAuthirtyLogin();
			sal.setAppId("IM");
			sal.setLoginName("admin");
			String result=SSOAuthorityOperator.getInstance().login(sal);
			System.out.println("登录是否返回结果："+result);

		} catch (SSOAuthorityException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	//获取服务令牌
	@Test
	public  void testGetServiceTicket() {
		try {
			
			//先获取认证
			SSOAuthirtyLogin sal = new SSOAuthirtyLogin();
			sal.setAppId("IM");
			sal.setLoginName("admin");
			String mainTciket=SSOAuthorityOperator.getInstance().login(sal);
			//再获取服务令牌
			String result=SSOAuthorityOperator.getInstance().getServiceTicket(mainTciket);
			System.out.println("服务令牌有："+result);
		} catch (SSOAuthorityException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	//验证服务令牌是否已经过期
	@Test
	public void testIsAuthority() {
		try {
			
			//第一步，先获取认证
			SSOAuthirtyLogin sal = new SSOAuthirtyLogin();
			sal.setAppId("IM");
			sal.setLoginName("admin");
			String mainTciket=SSOAuthorityOperator.getInstance().login(sal);
			
			//第二步，获取服务令牌
			String serviceTicket=SSOAuthorityOperator.getInstance().getServiceTicket(mainTciket);
			//第三步，再验证服务令牌是否过期
			String result=SSOAuthorityOperator.getInstance().isAuthority("1111");
			System.out.println("验证令牌是否已经过期结果：(isAuthority为true则没有过期)\n"+result);
		} catch (SSOAuthorityException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	

}
