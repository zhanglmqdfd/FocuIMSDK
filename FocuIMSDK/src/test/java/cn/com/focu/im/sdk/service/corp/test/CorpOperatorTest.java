package cn.com.focu.im.sdk.service.corp.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.corp.CorpOperator;
import cn.com.focu.im.sdk.service.corp.entity.CorpAdd;
import cn.com.focu.im.sdk.service.corp.entity.CorpDelete;
import cn.com.focu.im.sdk.service.corp.entity.CorpInfo;
import cn.com.focu.im.sdk.service.corp.entity.CorpLiang;
import cn.com.focu.im.sdk.service.corp.entity.CorpSet;
import cn.com.focu.im.sdk.service.corp.entity.CorpUpdate;
import cn.com.focu.im.sdk.service.corp.exception.CorpAddException;
import cn.com.focu.im.sdk.service.corp.exception.CorpException;
import cn.com.focu.im.sdk.service.user.UserOperator;
import cn.com.focu.im.sdk.service.user.entity.UserRegister;

public class CorpOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	// 获取企业分类列表
	@Test
	public void testGetCorpCategiries() {
		String ss = CorpOperator.getInstance().getCorpCategiries();
		System.out.println("结果是：" + ss);
	}

	// 根据企业类别id获取该企业的级别
	@Test
	public void testGetCorpLevelByCateId() {
		try {
			String ss = CorpOperator.getInstance().getCorpLevelsByCateId(1);
			System.out.println("级别为是：" + ss);
		} catch (CorpException e) {
			System.out.println(e.getMessage());
		}

	}

	// 获取系统行业列表
	@Test
	public void testGetCorpIndustries() {
		try {
			String ss = CorpOperator.getInstance().getIndustries();
			System.out.println("系统行业列表有：" + ss);
		} catch (CorpException e) {
			System.out.println(e.getMessage());
		}

	}

	// 获取系统行业列表
	@Test
	public void testGetCorpAdd() {
		try {
			UserRegister register = new UserRegister();
			register.setDisplayName("测试账号33333");
			register.setRandom(true);
			register.setMd5(false);
			register.setPassword("1");
			Integer uid = UserOperator.getInstance().register(register);
			CorpAdd ca = new CorpAdd();
			ca.setUid(uid);
			ca.setAddress("斯泰普斯中心11111111");
			ca.setCity("洛杉矶");
			ca.setContact("篮球");
			ca.setCountry("Americal");
			ca.setCorpName("全球权威2222");
			ca.setEmail("jaajfalsfj@163.com");
			// ca.setEnabled("1");
			// ca.setEndTime("2011-01-01");
			ca.setFax("225562");
			ca.setIndustry(1);
			ca.setLevel(1);
			ca.setEnable(true);
			// ca.setIndustryID(2);
			// ca.setIntroduce("很好");
			ca.setLegal("kkoo");
			// ca.setLevelID(1);
			ca.setPostcode("255412");
			ca.setProvince("大洲省");
			// ca.setRegisteredTime("2010-01-01");
			// ca.setShortName("kobe");
			ca.setTel("15654563256");
			Integer ss = CorpOperator.getInstance().add(ca);
			System.out.println("添加企业结果(0成功-1失败)：" + ss);
		} catch (CorpAddException e) {
			System.out.println(e.getMessage());
		}
	}

	// 改变企业状态
	@Test
	public void testCorpChangeState() {
		try {
			Integer ss = CorpOperator.getInstance().changeState(1, 0);
			System.out.println("改变企业状态结果：(0成功，-1失败)" + ss);
		} catch (CorpException e) {
			System.out.println(e.getMessage());
		}
	}

	// 获取系统行业列表
	@Test
	public void testGetCorpUpdate() {
		try {
			CorpUpdate ca = new CorpUpdate();
			ca.setCorpId(1);
			ca.setAddress("斯泰普斯中心");
			ca.setCity("洛杉矶");
			ca.setContact("篮球");
			ca.setCountry("Americal");
			ca.setCorpName("默认企业");
			ca.setCorpCode("0002");
			ca.setEmail("jaajfalsfj@163.com");
			// ca.setEnabled("1");
			ca.setCloseTime("2020-01-01");
			ca.setFax("225562");
			ca.setIndustry(1);
			ca.setIntro("很好");
			ca.setLegal("kkoo");
			ca.setLevel(1);
			ca.setPostCode("255412");
			ca.setProvince("大洲省");
			ca.setDistrict("aaaaaa");
			ca.setOpenTime("2010-01-01");
			ca.setCorpSName("aaaaa");
			ca.setTel("15654563256");
			Integer ss = CorpOperator.getInstance().update(ca);
			System.out.println("添加更新结果(0成功-1失败)：" + ss);
		} catch (CorpException e) {
			System.out.println(e.getMessage());
		}
	}

	// 删除企业
	@Test
	public void testCorpDel() {
		try {
			CorpDelete cd = new CorpDelete();
			cd.setCorpId(new Integer[] { 2 });
			Integer ss = CorpOperator.getInstance().del(cd);
			System.out.println("删除企业结果：(0成功，-1失败)" + ss);
		} catch (CorpException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testGetSet() {
		CorpSet corpSet = CorpOperator.getInstance().getSet(1);
		System.out.println(corpSet.isAllowChangeDisplayName());
		System.out.println(corpSet.isAllowContact());
		System.out.println(corpSet.isAllowSearch());
		System.out.println(corpSet.isCorpUserSort());
		System.out.println(corpSet.isMiniEnable());
		System.out.println(corpSet.getMiniUrl());
		System.out.println(corpSet.getMiniParams());
		System.out.println(corpSet.getMiniWidth());
		System.out.println(corpSet.getMiniHeight());
	}

	@Test
	public void testChangeSet() {
		CorpSet corpSet = CorpOperator.getInstance().getSet(1);
		System.out.println(corpSet.isAllowChangeDisplayName());
		System.out.println(corpSet.isAllowContact());
		System.out.println(corpSet.isAllowSearch());
		System.out.println(corpSet.isCorpUserSort());
		corpSet.setAllowChangeDisplayName(false);
		corpSet.setAllowContact(true);
		corpSet.setAllowSearch(true);
		corpSet.setCorpUserSort(true);
		corpSet.setMiniEnable(true);
		corpSet.setMiniUrl("http://baidu.com/");
		corpSet.setMiniParams("?xxxx=xxx");
		corpSet.setMiniWidth(700);
		corpSet.setMiniHeight(400);
		CorpOperator.getInstance().changeSet(corpSet);
	}

	@Test
	public void testCorpLiang() {
		System.out.println(CorpOperator.getInstance().corpLiang(
				new CorpLiang(3, "205727", "95599")));
	}

	@Test
	public void testCorpLiangBecome() {
		System.out.println(CorpOperator.getInstance().corpLiangBecome(
				new CorpLiang(3, "205727")));
	}

	@Test
	public void testGuideComplete() {
		System.out.println(CorpOperator.getInstance().guideComplete(
				"81ceb6f6-1292-4586-bcdf-8e9b45c01e06"));
	}
	
	@Test
	public void testUpdate(){
		CorpUpdate cu = new CorpUpdate();
		CorpOperator.getInstance().update(cu);
	}
	
	@Test
	public void testGetCorpInfo(){
		CorpInfo corpInfo = new CorpInfo();
//		corpInfo.setCorpId(1);
		corpInfo.setCorpCode("1111");
		corpInfo.setFlag(1);
		String str  = CorpOperator.getInstance().getCorpInfo(corpInfo);
		System.out.println(str);
	}
}
