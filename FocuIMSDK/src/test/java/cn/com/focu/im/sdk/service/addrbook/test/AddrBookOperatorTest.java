package cn.com.focu.im.sdk.service.addrbook.test;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.addrbook.AddrBookOperator;
import cn.com.focu.im.sdk.service.addrbook.entity.AddBranchsToCorp;
import cn.com.focu.im.sdk.service.addrbook.entity.AddrBookAdd;
import cn.com.focu.im.sdk.service.addrbook.entity.ArrowBranchs;
import cn.com.focu.im.sdk.service.addrbook.entity.ArrowUser;
import cn.com.focu.im.sdk.service.addrbook.entity.BranchUsers;
import cn.com.focu.im.sdk.service.addrbook.entity.Branchs;
import cn.com.focu.im.sdk.service.addrbook.entity.DeleteBranchsFromCorp;
import cn.com.focu.im.sdk.service.addrbook.entity.RegulationBranchs;
import cn.com.focu.im.sdk.service.addrbook.entity.RegulationCorpUserFromBranch;
import cn.com.focu.im.sdk.service.addrbook.entity.UpdateBranchsInfo;
import cn.com.focu.im.sdk.service.addrbook.exception.AddrBookException;

public class AddrBookOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	// 添加人员到部门(即添加企业人员)
	@Test
	public void testJoinToBranch() {
		try {
			AddrBookAdd aba = new AddrBookAdd();
			aba.setCorpId(1);
			aba.setBcode("DEFAULT");
			aba.setLoginName(new String[] { "test1" });
			// aba.setUid(new Integer[] { 117 });
			Integer res = AddrBookOperator.getInstance().joinToBranchs(aba);
			System.out.println("执行结果：(1成功-1失败)" + res);
		} catch (AddrBookException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testRegulationCorpUserFromBranch() {
		RegulationCorpUserFromBranch regulationCorpUserFromBranch = new RegulationCorpUserFromBranch(
				"0001", "test2");
		regulationCorpUserFromBranch.setLoginName(new String[] { "test2" });
		AddrBookOperator.getInstance().regulationCorpUserFromBranch(
				regulationCorpUserFromBranch);
	}

	@Test
	public void testRegulationBranchs() {
		RegulationBranchs regulationBranchs = new RegulationBranchs(
				"81ceb6f6-1292-4586-bcdf-8e9b45c01e06", "333333", "222222");
		AddrBookOperator.getInstance().regulationBranchs(regulationBranchs);
	}

	@Test
	public void testAddBranchsToCorp() {
		AddrBookOperator.getInstance().addBranchsToCorp(
				new AddBranchsToCorp(1, "test1", "test1", "DEFAULT"));
	}

	@Test
	public void testUpdateBranchInfo() {
		AddrBookOperator.getInstance().updateBranchInfo(
				new UpdateBranchsInfo("910386c9-3897-4239-9bd4-7035bf507d3a",
						"test1", "test001"));
	}

	@Test
	public void testIsBranchsExists() {
		AddrBookOperator.getInstance().isBranchsExists(1, "0");
	}

	@Test
	public void testDeleteBranchsFromCorp() {
		DeleteBranchsFromCorp delete = new DeleteBranchsFromCorp();
		delete.setCorpId(1);
		delete.setBcode("cccc");

		AddrBookOperator.getInstance().deleteBranchsFromCorp(delete);
	}

	@Test
	public void testSyncToClient() {
		AddrBookOperator.getInstance().syncToClient("0001");
	}

	@Test
	public void testArrowBranchs() {
		AddrBookOperator.getInstance().arrowBranchs(
				new ArrowBranchs("B", "910386c9-3897-4239-9bd4-7035bf507d3a",
						"aaaaad"));
	}

	@Test
	public void testArrowUser() {
		AddrBookOperator.getInstance().arrowUser(
				new ArrowUser("U", "1000011", "1000015"));
	}

	@Test
	public void testGetCorpbranchsByPid() {
		Branchs branchs = new Branchs();
		branchs.setCorpId(1);
		branchs.setParentId(0);

		AddrBookOperator.getInstance().getCorpbranchsByPid(branchs);
	}

	@Test
	public void testGetBranchUsersByBid() {
		BranchUsers branchUsers = new BranchUsers();
		branchUsers.setCorpId(1);
		branchUsers.setBid(1);
		AddrBookOperator.getInstance().getBranchUsersByBid(branchUsers);
	}

	@Test
	public void testBranchSort() {
		JSONArray jaArray = new JSONArray();

		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("branchCode", "2ad2594f3aaa47fda712adb718383281");
		jsonObj1.put("position", "1000");
		jaArray.add(jsonObj1);

		JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put("branchCode", "5e0ed78ce8d54f9ba286a983da5dce41");
		jsonObj2.put("position", "2000");
		jaArray.add(jsonObj2);

		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("branchCode", "11ddf1e1db154ad1b6371cef44ee1b20");
		jsonObj3.put("position", "3000");
		jaArray.add(jsonObj3);

		JSONObject jsonObj4 = new JSONObject();
		jsonObj4.put("branchCode", "d67d0a75aca54f8199506340558e6169");
		jsonObj4.put("position", "4000");
		jaArray.add(jsonObj4);

		int i = AddrBookOperator.getInstance().branchSort(jaArray.toString());
		System.out.println(i);
	}

	@Test
	public void testCorpUserSort() {
		JSONArray jaArray = new JSONArray();

		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("loginName", "test1");
		jsonObj1.put("position", "1000");
		jaArray.add(jsonObj1);

		JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put("loginName", "test1_1");
		jsonObj2.put("position", "2000");
		jaArray.add(jsonObj2);

		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("loginName", "test1_2");
		jsonObj3.put("position", "3000");
		jaArray.add(jsonObj3);

		AddrBookOperator.getInstance().corpUserSort(jaArray.toString());
	}
}
