package cn.com.focu.im.sdk.service.disk.test;

import java.io.File;

import org.junit.Test;

import cn.com.focu.im.sdk.service.disk.NDiskOperator;
import cn.com.focu.im.sdk.service.disk.entity.DeleteDirectory;
import cn.com.focu.im.sdk.service.disk.entity.NDiskEntity;
import cn.com.focu.im.sdk.service.disk.entity.NewDirectory;
import cn.com.focu.im.sdk.service.disk.entity.RenameDirectory;

public class NDiskOperatorTest {

	@Test
	public void testFiles() {
		NDiskOperator.getInstence().files(NDiskEntity.buildForGND(27, 22L));
	}

	@Test
	public void testDownload() {
		File file = NDiskOperator.getInstence().download(150L);
		System.out.println("file size: " + file.length() + " byte");
	}

	@Test
	public void testUpload() {
		System.out.println(NDiskOperator.getInstence().upload(
				NDiskEntity.buildForGND(1, 27, 0L),
				new File[] { new File(
						"F:\\im\\21cp\\FocuIMSDK\\bin\\temp\\主网地区信息数据.xls") }));
	}
	
	@Test
	public void testNewDirectory(){
		NewDirectory newDirectory = new NewDirectory();
		newDirectory.setCreateId(4);
		newDirectory.setParentDir(15);
		
		NDiskOperator.getInstence().newDirectory(newDirectory);
	}
	
	@Test
	public void testRenameDirectory(){
		RenameDirectory renameDirectory = new RenameDirectory();
		renameDirectory.setDirId(18);
		renameDirectory.setNewName("newName");
		
		NDiskOperator.getInstence().renameDirectory(renameDirectory);
	}
	
	@Test
	public void testDeleteDirectory(){
		DeleteDirectory deleteDirectory = new DeleteDirectory();
		deleteDirectory.setDirId(19);
		deleteDirectory.setCurrentId(1);
		
		NDiskOperator.getInstence().deleteDirectory(deleteDirectory);
	}
}
