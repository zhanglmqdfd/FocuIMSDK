package cn.com.focu.im.sdk.service.industryApps.test;

import org.junit.Test;

import cn.com.focu.im.sdk.service.industryApps.IndustryAppsOperator;
import cn.com.focu.im.sdk.service.industryApps.entity.AddIndustryApps;
import cn.com.focu.im.sdk.service.industryApps.entity.DelIndustryApps;
import cn.com.focu.im.sdk.service.industryApps.entity.IndustryApps;
import cn.com.focu.im.sdk.service.industryApps.entity.UpdateIndustryApps;


public class IndustryAppsOperatorTest {
	
	@Test
	public void testAddIndustryApps(){
		AddIndustryApps addIndustryApps = new AddIndustryApps();
		addIndustryApps.setIndustryName("test5");
		addIndustryApps.setMenuName("test5");
		addIndustryApps.setImage("http://www.baidu.com/image");
		addIndustryApps.setWidth(400);
		addIndustryApps.setHeight(500);
		addIndustryApps.setUrl("www.baidu.com");
		addIndustryApps.setAdditional("111");
		
		IndustryAppsOperator.getInstence().addIndustryApps(addIndustryApps);
	}
	
	@Test
	public void testUpdateIndustryApps(){
		UpdateIndustryApps updateIndustryApps = new UpdateIndustryApps();
		
		updateIndustryApps.setId(4);
		updateIndustryApps.setIndustryName("test5x5");
		updateIndustryApps.setMenuName("test5x5");
		updateIndustryApps.setImage("http://www.5x5.com/image");
		updateIndustryApps.setWidth(400);
		updateIndustryApps.setHeight(500);
		updateIndustryApps.setUrl("www.5x5.com");
		updateIndustryApps.setAdditional("5x5");
		
		IndustryAppsOperator.getInstence().updateIndustryApps(updateIndustryApps);
	}
	
	@Test
	public void testDelIndustryApps(){
		DelIndustryApps delIndustryApps = new DelIndustryApps();
		Integer[] appIds = { 4, 3 };
		
		delIndustryApps.setAppIds(appIds);
		
		IndustryAppsOperator.getInstence().delIndustryApps(delIndustryApps);
	}
	
	@Test
	public void testGetIndustryApps(){
		IndustryApps app = new IndustryApps();
		app.setPageNo(1);
		app.setPageSize(10);
		IndustryAppsOperator.getInstence().getIndustryApps(app);
	}
}
