package cn.com.focu.im.sdk.service.pendingApp.test;

import org.junit.Test;

import cn.com.focu.im.sdk.service.pendingApp.PendingAppOperator;
import cn.com.focu.im.sdk.service.pendingApp.entity.AddPending;
import cn.com.focu.im.sdk.service.pendingApp.entity.AddPendingState;
import cn.com.focu.im.sdk.service.pendingApp.entity.DeletePending;
import cn.com.focu.im.sdk.service.pendingApp.entity.GetPendingApp;

public class PendingAppOperatorTest {
	@Test
	public void testAddPendingApp() {
		AddPending pending = new AddPending();
		pending.setAppId(1);
		pending.setAutoComplete(true);
		pending.setContent("test1");
		pending.setFlag(0);
		pending.setLoginName("test1");
		pending.setSubject("test1");
		pending.setToAppUrl("www.baidu.com");
		pending.setSecureKey("111111");
		System.out.print(PendingAppOperator.getInstence().add(pending));
	}

	@Test
	public void testDeletePendingApp() {
		DeletePending pending = new DeletePending();
		pending.setAppId(1);
		pending.setFlag(0);
		pending.setLoginName("admin");
		pending.setDeleteAll(false);
		pending.setSecureKey("20120321");
		pending.setPendingId(new Integer[] { 14, 48 });
		System.out.print(PendingAppOperator.getInstence().delete(pending));
	}

	@Test
	public void testAddStatePendingApp() {
		AddPendingState addPendingState = new AddPendingState();
		addPendingState.setAppId(1);
		addPendingState.setState("发的是飞洒发第三方");
		addPendingState.setPendingId(16);
		addPendingState.setComplete(true);
		addPendingState.setSecureKey("20120321");
		System.out.print(PendingAppOperator.getInstence().addState(
				addPendingState));
	}

	@Test
	public void testGetPendingAppReturnPage() {
		GetPendingApp getPendingApp = new GetPendingApp();
		getPendingApp.setLoginName("admin");
		getPendingApp.setFlag(0);
		getPendingApp.setPageNo(1);
		getPendingApp.setPageSize(20);

		String str = PendingAppOperator.getInstence().getPendingAppReturnPage(
				getPendingApp);

		System.out.println(str);
	}
}
