package cn.com.focu.im.sdk.service.sysmessage.test;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.msg.SysMessageOperator;
import cn.com.focu.im.sdk.service.msg.entity.SysMessage;
import cn.com.focu.im.sdk.service.msg.exception.SysMessageException;

public class SysMessageOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	//发送消息
	@Test
	public void testSendMessageToUsers() {
		try {
			SysMessage sm = new SysMessage();
			sm.setContent("asdf");
			sm.setHeight(111);
			sm.setWidth(222);
			sm.setReceivers("admin");
			sm.setTitle("测试");
			int intresult = SysMessageOperator.getInstance()
					.sendMessageToUsers(sm);
			System.out.println("消息发送结果(0成功-1失败)：" + intresult);
		} catch (SysMessageException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testSaveCorpAnnounce() {
		try {
			SysMessage sm = new SysMessage();
			sm.setAnnounceType(1);
			sm.setCloseDelay(0);
			sm.setContent("测试的消息内容9888888");
			sm.setFrequency(1);
			sm.setHeight(200);
			sm.setWidth(200);
			sm.setPosition(1);
			sm.setReceivers("admin");
			sm.setTitle("测试的消息标题988888");
			sm.setShowDelay(1);
			sm.setReceiveType(2);
//			sm.setSender("1");
			sm.setEffectiveDate("2012-01-01");
			sm.setExpiredDate("2013-09-09");
			sm.setAnnReceiveType(0);
			sm.setRealTime(false);
			int intresult = SysMessageOperator.getInstance().saveCorpAnnounce("222", sm);
			System.out.println("结果(0成功-1失败)：" + intresult);
		} catch (SysMessageException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//发送消息
	@Test
	public void testGetMessages() {
		try {
			String intresult = SysMessageOperator.getInstance().getMessages("jstx", 1, 5, 0);
			System.out.println("结果：" + intresult);
		} catch (SysMessageException e) {
			System.out.println(e.getMessage());
		}
	}

	/*@Test
	public void testGetCorpAnnounceInfoById() {
		try {
			String intresult = SysMessageOperator.getInstance().getCorpAnnounceInfoById(1);
			System.out.println("结果：" + intresult);
		} catch (SysMessageException e) {
			System.out.println(e.getMessage()+"**");
		}
	}*/
	
}
