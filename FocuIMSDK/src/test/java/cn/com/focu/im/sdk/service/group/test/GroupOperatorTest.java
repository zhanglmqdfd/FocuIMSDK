package cn.com.focu.im.sdk.service.group.test;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.group.GroupOperator;
import cn.com.focu.im.sdk.service.group.SelfGroupOperator;
import cn.com.focu.im.sdk.service.group.entity.GroupCreate;
import cn.com.focu.im.sdk.service.group.entity.GroupSort;
import cn.com.focu.im.sdk.service.group.entity.GroupUpdate;
import cn.com.focu.im.sdk.service.group.entity.GroupUserAdd;
import cn.com.focu.im.sdk.service.group.entity.GroupUserDelete;
import cn.com.focu.im.sdk.service.group.entity.Groups;
import cn.com.focu.im.sdk.service.group.entity.SelfGroup;
import cn.com.focu.im.sdk.service.group.exception.GroupCreateException;
import cn.com.focu.im.sdk.service.group.exception.GroupException;
import cn.com.focu.im.sdk.service.group.exception.GroupUpdateException;
import cn.com.focu.im.sdk.service.group.exception.GroupUserAddException;

public class GroupOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testGetGroupTypes() {
		String grouptype = GroupOperator.getInstence().getTypes(1);
		System.out.println("根据一级分类id查找二级分类：" + grouptype);
	}

	@Test
	public void testGetGroupCategories() {
		String groupCategories = GroupOperator.getInstence()
				.getCategories();
		System.out.println("群集合有：" + groupCategories);
	}

	// 根据群分类获取群级别列表
	@Test
	public void testGroupLevelsByCateId() {
		String groupCategories = GroupOperator.getInstence()
				.getLevelsByCateId(1);
		System.out.println("1级别群集合有：" + groupCategories);
	}

	// 创建新群
	@Test
	public void testGroupCreate() {
		try {
			GroupCreate gc = new GroupCreate();
			gc.setGname("测试群5");
			gc.setGlevel(2);
			gc.setGtype(1);
			gc.setIntro("测试群5");
			gc.setNotice("测试群5");
			gc.setValidate(0);
			gc.setLoginName("admin");
//			gc.setUid(1000003);
//			gc.setUsers(new String[] { "13004" });
			Integer res = GroupOperator.getInstence().create(gc);
			System.out.println("创建群成功，群id为" + res);
		} catch (GroupCreateException e) {
			System.out.println(e.getMessage());
		}
	}

	// 更改群信息
	@Test
	public void testGroupUpdate() {
		try {
			GroupUpdate gu = new GroupUpdate();
			gu.setGname("holder群");
			gu.setGid(14);
			gu.setGlevel(2);
			gu.setGtype(1);
			gu.setIntro("呵呵哈哈");
			gu.setNotice("no notice");
			gu.setValidate(1);
			gu.setUid(3);
			Integer res = GroupOperator.getInstence().update(gu);
			System.out.println("更新群结果:(1为成功,0失败)" + res);
		} catch (GroupUpdateException e) {
			System.out.println(e.getMessage());
		}
	}

	// 删除群
	@Test
	public void testGroupDel() {
		try {
			Integer res = GroupOperator.getInstence().del(5);
			System.out.println("删除群结果:(1为成功,0失败)" + res);
		} catch (GroupException e) {
			System.out.println(e.getMessage());
		}
	}

	// 获得groupid下的群的用户
	@Test
	public void testGetUser() {
		try {
			String res = GroupOperator.getInstence().getUsers(1);
			System.out.println("groupid为1的群成员有:" + res);
		} catch (GroupException e) {
			System.out.println(e.getMessage());
		}
	}

	// 获得groupid下的群的用户
	@Test
	public void testdelUser() {
		try {
			GroupUserDelete gud = new GroupUserDelete();
			gud.setFlag(1);
			gud.setGroups(new Integer[] { 15 });
			gud.setUsers(new Integer[] { 2, 1 });
			Integer res = GroupOperator.getInstence().delUsers(gud);
			System.out.println("删除结果(1成功0失败):" + res);
		} catch (GroupException e) {
			System.out.println(e.getMessage());
		}
	}

	// 添加群用户
	@Test
	public void testAddUser() {
		try {
			GroupUserAdd gud = new GroupUserAdd();
			gud.setFlag(0);
			gud.setGroups(new Integer[] { 15 });
			gud.setUsers(new Integer[] { 1 });
			Integer res = GroupOperator.getInstence().addUsers(gud);
			System.out.println("添加返回结果(1成功0失败):" + res);
		} catch (GroupUserAddException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testGetByType() {
		String str = GroupOperator.getInstence().getByType(20, 1, 1);
		System.out.println(str);
	}
	
	@Test
	public void testGetByKeyWord(){
		String str = GroupOperator.getInstence().getByKeyWord(20, 1, "test");
		System.out.println(str);
	}
	
	@Test
	public void testGetByProvince(){
		String str = GroupOperator.getInstence().getByProvince(2, 1, 9);
		System.out.println(str);
	}
	
	@Test
	public void testGetByCity(){
		String str = GroupOperator.getInstence().getByCity(1, 10, 1);
		System.out.println(str);
	}
	
	@Test
	public void testGroupSortByType(){
		GroupSort gs = new GroupSort();
		gs.setmGroupId(1);
		gs.setyGroupId(2);
		gs.setTypeId(3);
		gs.setOrientation(0);
		GroupOperator.getInstence().groupSortByType(gs);
	}
	
	@Test
	public void testGroupSortByMarket(){
		GroupSort gs = new GroupSort();
		gs.setmGroupId(1);
		gs.setyGroupId(2);
		gs.setOrientation(1);
		gs.setBz(1);
		GroupOperator.getInstence().groupSortByMarketOrProvinceSort(gs);
	}
	
	@Test
	public void testGetGroupByParentTypes(){
		Groups group = new Groups();
		group.setParentId(1);
		group.setPageNo(1);
		group.setPageSize(10);
		GroupOperator.getInstence().getGroupByParentTypes(group);
	}
	
	@Test
	public void testGetSelfGroupByPid(){
		SelfGroup sg = new SelfGroup();
		sg.setUid(1);
		sg.setParentid(-2147483648);
		
		SelfGroupOperator.getInstence().getSelfGroupByPid(sg);
	}
	
	@Test
	public void testUpdate(){
		
		
	}
}
