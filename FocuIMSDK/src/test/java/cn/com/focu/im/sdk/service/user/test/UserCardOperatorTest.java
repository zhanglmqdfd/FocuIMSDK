package cn.com.focu.im.sdk.service.user.test;

import net.sf.json.JSONObject;

import org.junit.BeforeClass;
import org.junit.Test;

import sun.misc.UCDecoder;

import cn.com.focu.im.sdk.service.user.UserCardOperator;
import cn.com.focu.im.sdk.service.user.entity.UserCardAdd;
import cn.com.focu.im.sdk.service.user.entity.UserCardDelete;
import cn.com.focu.im.sdk.service.user.entity.UserCardGet;
import cn.com.focu.im.sdk.service.user.entity.UserCardUpdate;
import cn.com.focu.im.sdk.service.user.exception.UserCardAddException;
import cn.com.focu.im.sdk.service.user.exception.UserCardDeleteException;
import cn.com.focu.im.sdk.service.user.exception.UserCardException;
import cn.com.focu.im.sdk.service.user.exception.UserCardUpdateException;

public class UserCardOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testGetCardTypes() {
		System.out.println(UserCardOperator.getInstance().getCardTypes());
	}

	@Test
	public void testGet() {
		try {
			UserCardGet ug = new UserCardGet();
			ug.setTid(1);
			ug.setUid(2);
			String rs = UserCardOperator.getInstance().get(ug);
			System.out.println("返回的卡片信息如下：" + rs);
		} catch (UserCardException e) {
			System.out.println(e.getMessage());
		}

	}

	@Test
	public void testDelete() {
		try {
			UserCardDelete ud = new UserCardDelete();
			ud.setTid(3);
			// ud.setGid(1);
			ud.setUid(2);
			UserCardOperator.getInstance().delete(ud);
			System.out.println("删除成功");
		} catch (UserCardDeleteException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testAdd() {
		try {
			UserCardAdd ud = new UserCardAdd();
			ud.setTid(3);
			// ud.setGid(1);
			ud.setUid(2);
			Integer re = UserCardOperator.getInstance().add(ud);
			System.out.println("添加结果(1成功，-1参数不正确，0未知异常)" + re);
		} catch (UserCardAddException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testUpdate() {
		try {
			UserCardUpdate ud = new UserCardUpdate();
			ud.setTid(1);
			// ud.setGid(1);
			ud.setUid(1);
			ud.setTrueName("kobebryant");
			ud.setBirthday("1988-05-17");
			ud.setBranch("aaaa/bbbb/cccc");
			ud.setDuty("测试");
			ud.setTel("1234567890");
			ud.setMobile("15000001111");
			ud.setFax("1234567890");
			ud.setEmail("test@163.com");
			UserCardOperator.getInstance().update(ud);
			System.out.println("更新成功！！");
		} catch (UserCardUpdateException e) {
			System.out.println(e.getMessage());
		}
	}

}
