package cn.com.focu.im.sdk.service.user.test;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.user.UserStateOperator;
import cn.com.focu.im.sdk.service.user.entity.UserStateGet;
import cn.com.focu.im.sdk.service.user.exception.UserStateGetException;

public class UserStateOperatorTest {

	 
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testGetStateTypes() {
		System.out.println("状态类型有："+UserStateOperator.getinstance().getStateTypes());
	}
	
	@Test
	public void testGet() {
		try {
			UserStateGet usg=new UserStateGet();
			//usg.setFlag(0);
			usg.setLoginName(new String[]{"admin"});
//			usg.setUid(new Integer[] {1,2});
			System.out.println("该用户状态为："+UserStateOperator.getinstance().get(usg));
		} catch (UserStateGetException e) {
			System.out.println(e.getMessage());
		}
		
	}
	

}
