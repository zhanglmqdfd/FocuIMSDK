package cn.com.focu.im.sdk.service.group.test;

import org.junit.Test;

import cn.com.focu.im.sdk.service.group.GroupNavbarOperator;
import cn.com.focu.im.sdk.service.group.GroupOperator;
import cn.com.focu.im.sdk.service.group.entity.GroupCollect;
import cn.com.focu.im.sdk.service.group.entity.GroupCurrentSizeUpdate;
import cn.com.focu.im.sdk.service.group.entity.GroupHotCreator;
import cn.com.focu.im.sdk.service.group.entity.GroupHotCreatorSort;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbar;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarCurrentSizeUpdate;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarRelations;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarRelationsSort;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarSort;

public class GroupNavbarOperatorTest {

	/**
	 * 测试添加群导航栏
	 */
	@Test
	public void addGroupNavbarTest() {
		GroupNavbar gn = new GroupNavbar();
		gn.setGroupNavbarName("VIP超级用户群2");
		gn.setFlg(1);
		GroupNavbarOperator.getInstence().addGroupNavbar(gn);

	}

	/**
	 * 测试删除群导航栏
	 */
	@Test
	public void delGroupNavbarTest() {
		GroupNavbar gn = new GroupNavbar();
		gn.setId(8);
		GroupNavbarOperator.getInstence().delGroupNavbar(gn);
	}

	/**
	 * 测试群导航栏排序
	 */
	@Test
	public void groupNavbarSortTest() {
		GroupNavbarSort gns = new GroupNavbarSort();
		gns.setOrientation(2);
		gns.setmNavbarId(7);
		gns.setFlg(-1);
		// gns.setyNavbarId(8);
		GroupNavbarOperator.getInstence().groupNavbarSort(gns);
	}

	/**
	 * 测试得到所有群导航栏
	 */
	@Test
	public void getGroupNavbarReturnPageTest() {
		GroupNavbar gn = new GroupNavbar();
		gn.setPageNo(1);
		gn.setPageSize(10);
		GroupNavbarOperator.getInstence().getGroupNavbarReturnPage(gn);
	}

	/**
	 * 测试添加群组至群导航栏下
	 */
	@Test
	public void addGroupToNavbarTest() {
		GroupNavbarRelations gnr = new GroupNavbarRelations();
		Integer[] groupIds = { 4 };
		gnr.setNavbarId(6);
		gnr.setGroupId(groupIds);
		GroupNavbarOperator.getInstence().addGroupToNavbar(gnr);
	}

	/**
	 * 测试删除导航栏下的群组
	 */
	@Test
	public void delGroupNavbarRelationsTest() {
		GroupNavbarRelations gnr = new GroupNavbarRelations();
		Integer[] groupIds = { 1, 2 };
		gnr.setNavbarId(5);
		gnr.setGroupId(groupIds);
		GroupNavbarOperator.getInstence().delGroupNavbarRelations(gnr);
	}

	/**
	 * 测试各群组排序
	 */
	@Test
	public void GroupNavbarRelationsSortTest() {
		GroupNavbarRelationsSort gnrs = new GroupNavbarRelationsSort();
		gnrs.setmGroupId(4);
		gnrs.setyGroupId(2);
		gnrs.setNavbarId(5);
		gnrs.setOrientation(0);
		GroupNavbarOperator.getInstence().GroupNavbarRelationsSort(gnrs);
	}

	/**
	 * 测试分页得到所有导航栏下的群组
	 */
	@Test
	public void getGroupNavbarRelationsReturnPageTest() {
		GroupNavbarRelations gnr = new GroupNavbarRelations();
		gnr.setPageNo(1);
		gnr.setPageSize(20);
		gnr.setNavbarId(1);
		GroupNavbarOperator.getInstence()
				.getGroupNavbarRelationsReturnPage(gnr);
	}

	/**
	 * 测试修改群在线人数和总人数
	 */
	@Test
	public void updateGroupCurrentSizeTest() {
		GroupCurrentSizeUpdate gcsu = new GroupCurrentSizeUpdate();
		gcsu.setGroupId(2);
		gcsu.setNavbarId(4);
		gcsu.setOnline(100);
		gcsu.setCurrentSize(230);
		GroupNavbarOperator.getInstence().updateGroupCurrentSize(gcsu);
	}

	/**
	 * 测试根据群聊天数量得到热门群组
	 */
	@Test
	public void addHotGroupToNavbarTest() {
		GroupNavbarOperator.getInstence().addHotGroupToNavbar(4);
	}

	/**
	 * 测试得到超人气群主
	 */
	@Test
	public void getHotGroupCreatorTest() {
		GroupHotCreator ghc = new GroupHotCreator();
		ghc.setPageNo(1);
		ghc.setPageSize(10);

		GroupNavbarOperator.getInstence().getHotGroupCreator(ghc);
	}

	/**
	 * 测试添加超人去群主
	 */
	@Test
	public void addGroupHotCreatorTest() {
		GroupHotCreator ghc = new GroupHotCreator();
		Integer[] userId = { 1, 2, 3 };
		ghc.setUserId(userId);
		GroupNavbarOperator.getInstence().addGroupHotCreator(ghc);
	}

	/**
	 * 测试删除超人气群主
	 */
	@Test
	public void delGroupHotCreatorTest() {
		GroupHotCreator ghc = new GroupHotCreator();
		Integer[] ids = { 4, 5, 6 };
		ghc.setIds(ids);
		GroupNavbarOperator.getInstence().delGroupHotCreator(ghc);
	}

	/**
	 * 测试超人气群主排序
	 */
	@Test
	public void groupHotCreatorSortTest() {
		GroupHotCreatorSort ghcs = new GroupHotCreatorSort();
		ghcs.setmUserId(2);
		ghcs.setyUserId(1);
		ghcs.setOrientation(2);
		GroupNavbarOperator.getInstence().groupHotCreatorSort(ghcs);
	}
	
	/**
	 * 得到所有群主
	 */
	@Test
	public void getGroupCreatorTest(){
		GroupHotCreator ghc = new GroupHotCreator();
		ghc.setPageNo(1);
		ghc.setPageSize(10);
		
		String str = GroupNavbarOperator.getInstence().getGroupCreator(ghc);
		System.out.println(str);
	}
	
	/**
	 * 修改导航栏名称
	 */
	@Test
	public void modifNavbarNameTest(){
		GroupNavbar gn = new GroupNavbar();
		gn.setId(7);
		gn.setGroupNavbarName("公告活动区1");
		GroupNavbarOperator.getInstence().modifNavbarName(gn);
		
	}
	
	/**
	 * 按规则自动生成热门群主
	 */
	@Test
	public void addHotGroupCreatorTest(){
		GroupNavbarOperator.getInstence().autoAddHotGroupCreator();
	}
	
	/**
	 * 按规则自动生成正在热聊群
	 */
	@Test
	public void getGroupByTimeTest(){
		GroupNavbarOperator.getInstence().autoAddHotTalkGroup(5);
	}
	
	/**
	 * 根据id得到导航栏名称
	 */
	@Test
	public void getGroupNavbarByIdTest(){
		GroupNavbar gn = new GroupNavbar();
		gn.setId(6);
		GroupNavbarOperator.getInstence().getGroupNavbarById(gn);
	}
	
	/**
	 * 自定义导航栏总人数
	 */
	@Test
	public void updateNavbarCurrentSizeTest(){
		GroupNavbarCurrentSizeUpdate gncsu = new GroupNavbarCurrentSizeUpdate();
		gncsu.setNavbarId(1);
		gncsu.setOnline(100);
		gncsu.setCurrentSize(200);
		
		GroupNavbarOperator.getInstence().updateNavbarCurrentSize(gncsu);
	}
	
	@Test
	public void getGroupCurrentSizeSum(){
		GroupNavbarRelations gnr = new GroupNavbarRelations();
		gnr.setNavbarId(1);
		int num = GroupNavbarOperator.getInstence().getGroupCurrentSizeSum(gnr);
		System.out.println(num);
	}
	
	/**
	 * 根据聊天记录数量得到推荐热门群
	 */
	@Test
	public void getAutoGroupTest(){
		GroupNavbar gn = new GroupNavbar();
		gn.setId(1);
		GroupNavbarOperator.getInstence().getAutoGroup(gn);
	}
	
	/**
	 * 得到自动排序的热门群主列表
	 */
	@Test
	public void getAutoHotGroupCreatorTest(){
		GroupNavbarOperator.getInstence().getAutoHotGroupCreator();
	}
	
	/**
	 * 我收藏的群列表
	 */
	@Test
	public void groupCollectListTest(){
		GroupCollect gc = new GroupCollect();
//		gc.setUserId(0);
		gc.setLoginName("admin");
		gc.setPageNo(1);
		gc.setPageSize(10);
		
		GroupNavbarOperator.getInstence().groupCollectList(gc);
	}
	
	/**
	 * 收藏群
	 */
	@Test
	public void collectGroupTest(){
		GroupCollect gc = new GroupCollect();
//		gc.setUserId(2);
		gc.setLoginName("admin");
		gc.setGroupId(2);
		
		GroupNavbarOperator.getInstence().collectGroup(gc);
	}
	
	/**
	 * 取消收藏的群
	 */
	@Test
	public void cancelCollectGroupTest(){
		GroupCollect gc = new GroupCollect();
//		gc.setUserId(2);
		gc.setLoginName("admin");
		gc.setGroupId(2);
		
		GroupNavbarOperator.getInstence().cancelCollectGroup(gc);
	}
}
