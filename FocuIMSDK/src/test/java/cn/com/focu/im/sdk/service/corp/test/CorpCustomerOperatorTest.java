package cn.com.focu.im.sdk.service.corp.test;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.csm.CorpCustomerOperator;
import cn.com.focu.im.sdk.service.csm.entity.CorpCustomer;
import cn.com.focu.im.sdk.service.csm.entity.CustomersReturnPage;

/**
 * 测试企业客服
 * 
 * @author Administrator
 * 
 */
public class CorpCustomerOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * 测试增加企业客服
	 */
	@Test
	public void testSaveCorpCustomer() {
		try {
			CorpCustomer corpCustomer = new CorpCustomer();
			String[] loginNumbers = { "1000015", "dsx" };
			corpCustomer.setCorpCode("81ceb6f6-1292-4586-bcdf-8e9b45c01e06");
			corpCustomer.setLoginNumbers(loginNumbers);
			System.out.println(CorpCustomerOperator.getInstance()
					.saveCorpCustomer(corpCustomer));
		} catch (Exception e) {
		}
	}

	/**
	 * 测试删除企业客服
	 */
	@Test
	public void testDelCorpCustomer() {
		try {
			CorpCustomer corpCustomer = new CorpCustomer();
			String[] loginNumbers = { "cy", "dsx" };
			corpCustomer.setCorpCode("81ceb6f6-1292-4586-bcdf-8e9b45c01e06");
			corpCustomer.setLoginNumbers(loginNumbers);
			System.out.println(CorpCustomerOperator.getInstance()
					.delCorpCustomer(corpCustomer));
		} catch (Exception e) {
		}
	}

	/**
	 * 测试分页得到企业客服
	 */
	@Test
	public void getCustomersReturnPage() {
		try {
			CustomersReturnPage page = new CustomersReturnPage();
			page.setPageNo(1);
			page.setPageSize(2);
			page.setCorpCode("81ceb6f6-1292-4586-bcdf-8e9b45c01e06");
			String str = CorpCustomerOperator.getInstance()
					.getCustomersReturnPage(page);
			System.out.println(str);
		} catch (Exception e) {
		}
	}

}
