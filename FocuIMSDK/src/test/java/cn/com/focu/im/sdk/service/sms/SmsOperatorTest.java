package cn.com.focu.im.sdk.service.sms;

import org.junit.Test;

import cn.com.focu.im.sdk.service.sms.entity.SmsEntity;

public class SmsOperatorTest {

	@Test
	public void testSend(){
		SmsEntity sms = new SmsEntity();
		sms.setSender("zhangsan1");
		sms.setCheckBalance(false);
		sms.setContent("测试短信");
		sms.setMobiles(new String[]{"15000801758"});
		sms.setReceivers(new String[]{""});
		sms.setCustomize(true);
		SmsOperator.getInstence().send(sms);
	}
	
}
