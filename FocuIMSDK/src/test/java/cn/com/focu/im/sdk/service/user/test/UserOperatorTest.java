package cn.com.focu.im.sdk.service.user.test;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.user.UserOperator;
import cn.com.focu.im.sdk.service.user.entity.UserDelete;
import cn.com.focu.im.sdk.service.user.entity.UserGet;
import cn.com.focu.im.sdk.service.user.entity.UserList;
import cn.com.focu.im.sdk.service.user.entity.UserLoginNameExists;
import cn.com.focu.im.sdk.service.user.entity.UserLoginNumberExists;
import cn.com.focu.im.sdk.service.user.entity.UserOnlineTime;
import cn.com.focu.im.sdk.service.user.entity.UserPasswd;
import cn.com.focu.im.sdk.service.user.entity.UserRegister;
import cn.com.focu.im.sdk.service.user.entity.UserUpdate;
import cn.com.focu.im.sdk.service.user.exception.AuthenticationException;
import cn.com.focu.im.sdk.service.user.exception.UserDeleteException;
import cn.com.focu.im.sdk.service.user.exception.UserGetException;
import cn.com.focu.im.sdk.service.user.exception.UserLoginNameExistsException;
import cn.com.focu.im.sdk.service.user.exception.UserLoginNumberExistsException;
import cn.com.focu.im.sdk.service.user.exception.UserOnlineTimeException;
import cn.com.focu.im.sdk.service.user.exception.UserPasswdException;
import cn.com.focu.im.sdk.service.user.exception.UserRegisterException;
import cn.com.focu.im.sdk.service.user.exception.UserUpdateException;

public class UserOperatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testRegister() {
		try {
			UserRegister userRegister = new UserRegister();
			userRegister.setLoginName("zhanglmqdfd");
			userRegister.setPassword("123456");
			userRegister.setRandom(true);
			userRegister.setMd5(false);
			userRegister.setEmail("test1@qq.com");
			userRegister.setAllowEmailLogin(true);
			userRegister.setAllowMobileLogin(true);
			userRegister.setMobile("15814430584");
			userRegister.setDisplayName("zhanglmqdfd");
			userRegister.setAge(18);
			userRegister.setBirthday("1992-09-23");
			userRegister.setSex(1);
			userRegister.setGrade(0);

			// userRegister.setDuty("管理员2");
			Integer uid = UserOperator.getInstance().register(userRegister);
			System.out.println("用户注册成功!uid:" + uid);
		} catch (UserRegisterException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testOnlineTime() {
		try {
			UserOnlineTime userOnlineTime = new UserOnlineTime();
			userOnlineTime.setLoginName("admin");
			userOnlineTime.setFlag(2);
			userOnlineTime.setUid(2);
			userOnlineTime.setLoginNumber("");
			System.out.println(UserOperator.getInstance().onlineTime(
					userOnlineTime));
		} catch (UserOnlineTimeException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testGet() {
		try {
			UserGet userGet = new UserGet();
			userGet.setUid(1);
			System.out.println(UserOperator.getInstance().get(userGet)
					.getLoginName());
		} catch (UserGetException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testPasswd() {
		try {
			UserPasswd userPasswd = new UserPasswd();
			userPasswd.setLoginName("admin");
			// userGet.setFlag(2);
			userPasswd.setUid(2);
			userPasswd.setLoginNumber("");
			userPasswd.setPassword("123426");
			UserOperator.getInstance().passwd(userPasswd);
		} catch (UserPasswdException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testUpdate() {
		try {
			UserUpdate userUpdate = new UserUpdate();
			userUpdate.setLoginName("admin");
			userUpdate.setDisplayName("adminss");
			userUpdate.setSex(2);
			userUpdate.setAge(21);
			userUpdate.setBirthday("1988-05-17");
			userUpdate.setMobile("15000000045");
			userUpdate.setEmail("6350298395@qq.com");
			// userUpdate.setGrade(12);
			UserOperator.getInstance().update(userUpdate);
		} catch (UserUpdateException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testDelete() {
		try {
			UserDelete userDelete = new UserDelete();
			userDelete.setLoginName(new String[] { "test1" });
			UserOperator.getInstance().delete(userDelete);
		} catch (UserDeleteException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testIsLoginNameExists() {
		try {
			// userLoginNameExists.setUid(1);
			System.out.println(UserOperator.getInstance().isLoginNameExists(
					new UserLoginNameExists("admin1")));
		} catch (UserLoginNameExistsException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testIsLoginNumberExists() {
		try {
			// userLoginNameExists.setUid(1);
			System.out.println(UserOperator.getInstance().isLoginNumberExists(
					new UserLoginNumberExists("205725")));
		} catch (UserLoginNumberExistsException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testAuthentication() {
		try {
			// userLoginNameExists.setUid(1);
			System.out.println(UserOperator.getInstance().authentication(
					"admin", "admin"));
		} catch (AuthenticationException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testAdministratorLogin() {
		try {
			System.out.println(UserOperator.getInstance().administratorLogin(
					"admin", "admin1"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Test
	public void testGetCustomUserListReturnPage() {
		UserList userList = new UserList();
		userList.setPageSize(10);
		userList.setPageNo(1);
		UserOperator.getInstance().getCustomUserListReturnPage(userList);
	}

	@Test
	public void testGetUidByAccount() {
		int uid = UserOperator.getInstance().getUidByAccount("100000000001");
		System.out.println(uid);
	}
}
