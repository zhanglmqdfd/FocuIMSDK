package cn.com.focu.im.sdk.service.apps.test;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.Test;

import cn.com.focu.im.sdk.service.apps.AppOperator;
import cn.com.focu.im.sdk.service.apps.entity.AppOrder;

public class AppOperatorTest {

	@Test
	public void testSyncEvents() {
		try {
			AppOperator.getInstence().syncEvents(true, "admin", 1, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSyncEvents2() {
		JSONArray jaArray = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("loginName", "admin");
		jsonObj.put("event", 56);
		jaArray.add(jsonObj);

		JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put("loginName", "test1");
		jsonObj2.put("event", 99);
		jaArray.add(jsonObj2);

		AppOperator.getInstence().syncEvents2(false, 2, jaArray.toString(),true);
	}

	@Test
	public void testDeleteEventsByAppId() {
		AppOperator.getInstence().deleteEventsByAppId(false, 2);
	}
	
	@Test
	public void testOrder() {
		AppOrder order = new AppOrder();
		order.setLoginName("testgu");
		order.setAppId(1);
		AppOperator.getInstence().order(order);
	}
}
