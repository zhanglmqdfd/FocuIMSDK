package cn.com.focu.im.sdk.service.user.test;

import org.junit.BeforeClass;
import org.junit.Test;

import cn.com.focu.im.sdk.service.user.UserFriendOpterator;
import cn.com.focu.im.sdk.service.user.entity.UserFriendAdd;
import cn.com.focu.im.sdk.service.user.entity.UserFriendDelete;
import cn.com.focu.im.sdk.service.user.entity.UserFriendGet;
import cn.com.focu.im.sdk.service.user.entity.UserMessage;
import cn.com.focu.im.sdk.service.user.exception.UserFriendAddException;
import cn.com.focu.im.sdk.service.user.exception.UserFriendDeleteException;
import cn.com.focu.im.sdk.service.user.exception.UserFriendGetException;

public class UserFriendOpteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testGet() {
		try {
			UserFriendGet ufg = new UserFriendGet();
			ufg.setFlag(1);
			ufg.setUid(1);
			ufg.setSgid(1);
			// ufg.setStatleFlag(0);
			System.out.println("好友列表："
					+ UserFriendOpterator.getInstance().get(ufg));
		} catch (UserFriendGetException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testDelete() {
		try {
			UserFriendDelete ufg = new UserFriendDelete();
			// ufg.setUid(1);
			ufg.setLoginName("test1");
			ufg.setFlag(0);
			// ufg.setFid(2);
			ufg.setFname("tde");
			Integer res = UserFriendOpterator.getInstance().delete(ufg);
			System.out
					.println("返回结果参考\n\t1成功\n\t0删除用户失败，可能是该好友已经被删除，不存在\n\t-1要删除的用户还不是好友");
			System.out.println("删除后返回状态：" + res);
		} catch (UserFriendDeleteException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testAdd() {
		try {
			UserFriendAdd ufg = new UserFriendAdd();
			/*
			 * ufg.setUid(1); ufg.setFlag(1); ufg.setFid(45);
			 */
			// 给admin的帐号，添加好友tde
			ufg.setFlag(0);
			ufg.setLoginName("admin");
			ufg.setFname("tde");
			Integer re = UserFriendOpterator.getInstance().add(ufg);
			System.out.println("返回结果参考\n\t1成功\n\t0添加的用户不存在\n\t-1要添加的用户已经是好友");
			System.out.println("\n添加最后结果：" + re);
		} catch (UserFriendAddException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void testFriendMessage() {
		UserMessage uMessage = new UserMessage();
		uMessage.setUserLoginName("admin");
		uMessage.setFriendLoginName("test1");
		uMessage.setFlag(0);
		uMessage.setContent("测试");
		UserFriendOpterator.getInstance().friendMessage(uMessage);
	}

}
