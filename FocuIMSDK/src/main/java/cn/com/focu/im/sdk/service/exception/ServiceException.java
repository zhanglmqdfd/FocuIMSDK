package cn.com.focu.im.sdk.service.exception;

/**
 * 服务异常基类
 * 
 * @作者 yeabow
 * @创建日期 Jul 12, 2011 5:11:01 PM
 * @版本 v1.0.0
 */
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 2295845853626907133L;

	protected int code;

	public ServiceException() {
		super();
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
		this.code = -100002;
	}

	public ServiceException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public ServiceException(int code, String message) {
		super(message);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public ServiceException(String message) {
		super(message);
		this.code = -100001;
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}
}
