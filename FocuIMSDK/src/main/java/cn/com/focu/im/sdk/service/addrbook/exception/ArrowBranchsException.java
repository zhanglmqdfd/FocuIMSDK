package cn.com.focu.im.sdk.service.addrbook.exception;

public class ArrowBranchsException extends ArrowException{

	private static final long serialVersionUID = 7124399116832782654L;

	public ArrowBranchsException(int code, String message) {
		super(code, message);
	}

	public ArrowBranchsException(String message, Throwable cause) {
		super(message, cause);
	}
}
