package cn.com.focu.im.sdk.service.user.entity;

import net.sf.json.JSONObject;

public class User {
	private Integer id;
	private String loginName;
	private String loginNumber;
	private String headUrl;
	private String email;
	private String mobile;
	private String displayName;
	private String birthday;
	private Integer age;
	private Integer sex;
	private Integer grade;
	private Integer onlineTime;
	private String lastLoginTime;
	private String lastLoginIP;
	private Integer lastLoginType;
	private String lastLoginTypeName;
	
	public User(JSONObject result) {
		super();

		if (result.has("uid"))
			this.setId(result.getInt("uid"));
		if (result.has("displayName"))
			this.setDisplayName(result.getString("displayName"));
		if (result.has("age"))
			this.setAge(result.getInt("age"));
		if (result.has("sex"))
			this.setSex(result.getInt("sex"));
		if (result.has("headUrl"))
			this.setHeadUrl(result.getString("headUrl"));
		if (result.has("grade"))
			this.setGrade(result.getInt("grade"));
		if (result.has("birthday"))
			this.setBirthday(result.getString("birthday"));
		if (result.has("loginName"))
			this.setLoginName(result.getString("loginName"));
		if (result.has("loginNumber"))
			this.setLoginNumber(result.getString("loginNumber"));
		if (result.has("mobile"))
			this.setMobile(result.getString("mobile"));
		if (result.has("email"))
			this.setEmail(result.getString("email"));
		if (result.has("onlineTime"))
			this.setOnlineTime(result.getInt("onlineTime"));
		if (result.has("lastLoginTime"))
			this.setLastLoginTime(result.getString("lastLoginTime"));
		if (result.has("lastLoginIP"))
			this.setLastLoginIP(result.getString("lastLoginIP"));
		if (result.has("lastLoginType"))
			this.setLastLoginType(result.getInt("lastLoginType"));
		if (result.has("lastLoginTypeName"))
			this.setLastLoginTypeName(result.getString("lastLoginTypeName"));
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}
	
	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Integer getOnlineTime() {
		return onlineTime;
	}

	public void setOnlineTime(Integer onlineTime) {
		this.onlineTime = onlineTime;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIP() {
		return lastLoginIP;
	}

	public void setLastLoginIP(String lastLoginIP) {
		this.lastLoginIP = lastLoginIP;
	}

	public Integer getLastLoginType() {
		return lastLoginType;
	}

	public void setLastLoginType(Integer lastLoginType) {
		this.lastLoginType = lastLoginType;
	}

	public String getLastLoginTypeName() {
		return lastLoginTypeName;
	}

	public void setLastLoginTypeName(String lastLoginTypeName) {
		this.lastLoginTypeName = lastLoginTypeName;
	}
}
