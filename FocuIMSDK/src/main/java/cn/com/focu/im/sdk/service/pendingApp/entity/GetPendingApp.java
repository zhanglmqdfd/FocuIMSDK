package cn.com.focu.im.sdk.service.pendingApp.entity;

public class GetPendingApp {
	
	private Integer uid;
	private String loginName;
	private String loginNumber;
	private Integer flag;
	private Integer pageNo;
	private Integer pageSize;
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginNumber() {
		return loginNumber;
	}
	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
