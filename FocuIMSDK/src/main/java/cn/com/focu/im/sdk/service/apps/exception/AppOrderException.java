package cn.com.focu.im.sdk.service.apps.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class AppOrderException  extends ServiceException{

	private static final long serialVersionUID = 464426774091306370L;

	public AppOrderException(int code, String message) {
		super(code, message);
	}

	public AppOrderException(String message, Throwable cause) {
		super(message, cause);
	}
}
