package cn.com.focu.im.sdk.service.disk.entity;

public class NDiskEntity {

	private String type;
	private String loginName;
	private String loginNumber;
	private Integer uid;
	private Integer gid;
	private Long dir;
	private Integer flag;

	public static NDiskEntity buildForUND(Integer uid, Long dir) {
		NDiskEntity get = new NDiskEntity();
		get.uid = uid;
		get.dir = dir;
		get.type = "user";
		get.flag = 1;
		return get;
	}

	public static NDiskEntity buildForUNDName(String loginName, Long dir) {
		NDiskEntity get = new NDiskEntity();
		get.loginName = loginName;
		get.dir = dir;
		get.type = "user";
		get.flag = 0;
		return get;
	}

	public static NDiskEntity buildForUNDNumber(String loginNumber, Long dir) {
		NDiskEntity get = new NDiskEntity();
		get.loginNumber = loginNumber;
		get.dir = dir;
		get.type = "user";
		get.flag = 2;
		return get;
	}

	public static NDiskEntity buildForGND(Integer gid, Long dir) {
		NDiskEntity get = new NDiskEntity();
		get.dir = dir;
		get.type = "group";
		get.gid = gid;
		return get;
	}

	public static NDiskEntity buildForGND(Integer uid, Integer gid, Long dir) {
		NDiskEntity get = NDiskEntity.buildForUND(uid, dir);
		get.type = "group";
		get.gid = gid;
		return get;
	}

	public static NDiskEntity buildForGNDName(String loginName, Integer gid,
			Long dir) {
		NDiskEntity get = NDiskEntity.buildForUNDName(loginName, dir);
		get.type = "group";
		get.gid = gid;
		return get;
	}

	public static NDiskEntity buildForGNDNumber(String loginNumber,
			Integer gid, Long dir) {
		NDiskEntity get = NDiskEntity.buildForUNDNumber(loginNumber, dir);
		get.type = "group";
		get.gid = gid;
		return get;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getGid() {
		return gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public Long getDir() {
		return dir;
	}

	public void setDir(Long dir) {
		this.dir = dir;
	}
}
