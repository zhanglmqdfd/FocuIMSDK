package cn.com.focu.im.sdk.service.dic;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.corp.exception.CorpException;
import cn.com.focu.im.sdk.service.exception.ServiceException;

@Operator("dic")
public class DictionaryOperator extends ServiceOperator {
	private DictionaryOperator() {
	}

	private static class DictionaryOperatorHolder {
		static DictionaryOperator dictionaryOperator = new DictionaryOperator();
	}

	public static DictionaryOperator getInstance() {
		return DictionaryOperatorHolder.dictionaryOperator;
	}

	/**
	 * 获取所有国家信息
	 * 
	 * @return JSON数据格式类型列表
	 */
	public String getCountry() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			return result;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	/**
	 * 根据国家编号获取省份数据
	 * 
	 * @param countryId
	 *            国家编号
	 * @return 省份数据
	 */
	public String getProvince(Integer countryId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("countryId", countryId + "");
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			return result;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	/**
	 * 根据省份获取市区数据
	 * 
	 * @param provinceId
	 *            省份编号
	 * @return 市区数据
	 */
	public String getDistrict(Integer provinceId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("provinceId", provinceId + "");
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			return result;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	/**
	 * 根据市、区获取城市数据
	 * 
	 * @param districtId
	 *            市区
	 * @return 城市数据
	 */
	public String getCity(Integer districtId) {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("districtId", districtId + "");
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			return result;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}
	
	public String getMarket() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			return result;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}
}
