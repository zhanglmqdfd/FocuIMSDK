package cn.com.focu.im.sdk.service.user.entity;

public class UserStateGet {

	private Integer [] uid;
	private Integer flag =0;
	private String [] loginName;
	private String[] loginNumber;

	public Integer[] getUid() {
		return uid;
	}

	public void setUid(Integer[] uid) {
		this.flag = 1;
		this.uid = uid;
	}

	public Integer getFlag() {
		return flag;
	}

	public String[] getLoginName() {
		return loginName;
	}

	public void setLoginName(String[] loginName) {
		this.flag = 0;
		this.loginName = loginName;
	}

	public String[] getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String[] loginNumber) {
		this.flag = 2;
		this.loginNumber = loginNumber;
	}
}
