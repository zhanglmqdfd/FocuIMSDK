package cn.com.focu.im.sdk.service.user;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.user.entity.UserCardAdd;
import cn.com.focu.im.sdk.service.user.entity.UserCardDelete;
import cn.com.focu.im.sdk.service.user.entity.UserCardGet;
import cn.com.focu.im.sdk.service.user.entity.UserCardUpdate;
import cn.com.focu.im.sdk.service.user.exception.UserCardAddException;
import cn.com.focu.im.sdk.service.user.exception.UserCardDeleteException;
import cn.com.focu.im.sdk.service.user.exception.UserCardException;
import cn.com.focu.im.sdk.service.user.exception.UserCardUpdateException;

@Operator("ucard")
public class UserCardOperator extends ServiceOperator {
	private UserCardOperator() {
	}

	private static class UserCardOperatorHolder {
		static UserCardOperator userCardOperator = new UserCardOperator();
	}

	public static UserCardOperator getInstance() {
		return UserCardOperatorHolder.userCardOperator;
	}

	// 得到卡片类型
	public String getCardTypes() throws ServiceException {
		String rs;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			return rs;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}

	}

	// 删除卡片信息
	public Integer delete(UserCardDelete ucd) throws UserCardDeleteException {
		try {
			JSONObject json = JSONObject.fromObject(ucd);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}
			switch (status) {
			case 0:
				throw new UserCardDeleteException(status, "没有提供相应的参数");
			case -1:
				throw new UserCardDeleteException(status, "未知错误");
			default:
				throw new UserCardDeleteException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new UserCardDeleteException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new UserCardDeleteException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	// 添加新卡片信息
	public Integer add(UserCardAdd uca) throws UserCardAddException {
		try {
			JSONObject json = JSONObject.fromObject(uca);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}
			switch (status) {
			case 0:
				throw new UserCardAddException(status, "取值出现异常");
			case -1:
				throw new UserCardAddException(status, "没有提供相应的参数");
			default:
				throw new UserCardAddException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new UserCardAddException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new UserCardAddException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	};

	// 更新卡片信息
	public Integer update(UserCardUpdate ucu) throws UserCardUpdateException {
		try {
			JSONObject json = JSONObject.fromObject(ucu);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}
			switch (status) {
			case -1:
				throw new UserCardUpdateException(status, "要修改的用户可能不存在，无法完成更新");
			default:
				throw new UserCardUpdateException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new UserCardUpdateException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new UserCardUpdateException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	// 获得卡片信息
	public String get(UserCardGet uCardGet) throws UserCardException {
		try {
			JSONObject json = JSONObject.fromObject(uCardGet);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			try {
				JSONObject r = JSONObject.fromObject(rs);
				try {
					if (!r.getBoolean("success")) {
						if (!r.getBoolean("success")) {
							int error = r.getInt("error");
							switch (error) {
							case -1:
								throw new UserCardException(error, "帐号提供有误!");
							case 0:
								throw new UserCardException(error, "后台取值出现异常");
							default:
								throw new UserCardException(error, OTHER_ERROR);
							}
						}
					}
				} catch (JSONException e) {
				}
			} catch (JSONException e) {
				throw new UserCardException("返回数据格式有误!", e);
			}
			return rs;
		} catch (UnsupportedEncodingException e) {
			throw new UserCardException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}
}
