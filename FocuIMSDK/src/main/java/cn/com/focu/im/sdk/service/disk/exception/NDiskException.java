package cn.com.focu.im.sdk.service.disk.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class NDiskException extends ServiceException {

	private static final long serialVersionUID = 8139890252739800201L;

	public NDiskException(int code, String message) {
		super(code, message);
	}

	public NDiskException(String message, Throwable cause) {
		super(message, cause);
	}
}
