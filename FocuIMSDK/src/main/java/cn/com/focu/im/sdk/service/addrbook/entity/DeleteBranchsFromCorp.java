package cn.com.focu.im.sdk.service.addrbook.entity;

public class DeleteBranchsFromCorp {

	private String corpCode;

	private String bcode;
	
	private Integer corpId;

	public DeleteBranchsFromCorp(String corpCode, String bcode) {
		super();
		this.corpCode = corpCode;
		this.bcode = bcode;
	}
	
	public DeleteBranchsFromCorp(){
		
	}
	
	public Integer getCorpId() {
		return corpId;
	}

	public void setCorpId(Integer corpId) {
		this.corpId = corpId;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getBcode() {
		return bcode;
	}

	public void setBcode(String bcode) {
		this.bcode = bcode;
	}
}
