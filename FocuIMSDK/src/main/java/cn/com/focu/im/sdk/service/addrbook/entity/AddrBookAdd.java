package cn.com.focu.im.sdk.service.addrbook.entity;

public class AddrBookAdd {

	private String bcode;

	private Integer corpId;
	
	private String corpCode;

	private String[] loginName;

	private Integer[] uid;

	private String[] loginNumber;

	private Integer flag = 0;

	public String getBcode() {
		return bcode;
	}

	public void setBcode(String bcode) {
		this.bcode = bcode;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public void setCorpId(Integer corpId) {
		this.corpId = corpId;
	}

	public String[] getLoginName() {
		return loginName;
	}

	public void setLoginName(String[] loginName) {
		this.flag = 0;
		this.loginName = loginName;
	}

	public Integer[] getUid() {
		return uid;
	}

	public void setUid(Integer[] uid) {
		this.flag = 1;
		this.uid = uid;
	}

	public Integer getFlag() {
		return flag;
	}

	public String[] getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String[] loginNumber) {
		this.flag = 2;
		this.loginNumber = loginNumber;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}
}
