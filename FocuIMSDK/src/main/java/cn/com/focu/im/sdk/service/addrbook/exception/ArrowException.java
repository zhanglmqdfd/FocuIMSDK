package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class ArrowException extends ServiceException{

	private static final long serialVersionUID = -121387053117320077L;

	public ArrowException(int code, String message) {
		super(code, message);
	}

	public ArrowException(String message, Throwable cause) {
		super(message, cause);
	}
}
