package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserCardAddException extends ServiceException {

	private static final long serialVersionUID = -930658834596454145L;

	public UserCardAddException(int code, String message) {
		super(code, message);
	}

	public UserCardAddException(String message, Throwable cause) {
		super(message, cause);
	}
}					
