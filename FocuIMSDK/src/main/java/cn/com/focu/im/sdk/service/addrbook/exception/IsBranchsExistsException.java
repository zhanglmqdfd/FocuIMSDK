package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class IsBranchsExistsException extends ServiceException {

	private static final long serialVersionUID = 123349519095119567L;

	public IsBranchsExistsException(int code, String message) {
		super(code, message);
	}

	public IsBranchsExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
