package cn.com.focu.im.sdk.service.group.entity;

public class GroupNavbar {
	
	private Integer id;
	private String groupNavbarName;
	private Integer sort;
	private Integer flg; //0：系统群导航栏，不能删除、1：用户自定义导航栏。
	private Integer pageNo;
	private Integer pageSize;
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGroupNavbarName() {
		return groupNavbarName;
	}
	public void setGroupNavbarName(String groupNavbarName) {
		this.groupNavbarName = groupNavbarName;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Integer getFlg() {
		return flg;
	}
	public void setFlg(Integer flg) {
		this.flg = flg;
	}
	
}
