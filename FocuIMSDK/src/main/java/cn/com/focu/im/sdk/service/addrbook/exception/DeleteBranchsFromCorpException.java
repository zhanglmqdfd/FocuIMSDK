package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class DeleteBranchsFromCorpException extends ServiceException {

	private static final long serialVersionUID = -4399227781760977081L;

	public DeleteBranchsFromCorpException(int code, String message) {
		super(code, message);
	}

	public DeleteBranchsFromCorpException(String message, Throwable cause) {
		super(message, cause);
	}
}
