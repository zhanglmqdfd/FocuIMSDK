package cn.com.focu.im.sdk.service.corp.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class isCorpExistsException  extends ServiceException{
	
	private static final long serialVersionUID = -7103269301803037826L;

	public isCorpExistsException(int code, String message) {
		super(code, message);
	}

	public isCorpExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
