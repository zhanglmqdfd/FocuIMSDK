package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class AuthenticationException extends ServiceException {

	private static final long serialVersionUID = 6401170132693508703L;

	public AuthenticationException(int code, String message) {
		super(code, message);
	}

	public AuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}
}
