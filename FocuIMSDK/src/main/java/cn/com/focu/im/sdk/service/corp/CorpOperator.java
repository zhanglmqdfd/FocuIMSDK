package cn.com.focu.im.sdk.service.corp;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.corp.entity.CorpAdd;
import cn.com.focu.im.sdk.service.corp.entity.CorpDelete;
import cn.com.focu.im.sdk.service.corp.entity.CorpInfo;
import cn.com.focu.im.sdk.service.corp.entity.CorpLiang;
import cn.com.focu.im.sdk.service.corp.entity.CorpSet;
import cn.com.focu.im.sdk.service.corp.entity.CorpUpdate;
import cn.com.focu.im.sdk.service.corp.exception.CorpAddException;
import cn.com.focu.im.sdk.service.corp.exception.CorpException;
import cn.com.focu.im.sdk.service.corp.exception.isCorpExistsException;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.group.exception.GroupException;

@Operator("corp")
public class CorpOperator extends ServiceOperator {
	private CorpOperator() {
	}

	private static class CorpOperatorHolder {
		static CorpOperator corpOperator = new CorpOperator();
	}

	public static CorpOperator getInstance() {
		return CorpOperatorHolder.corpOperator;
	}

	public Integer changeState(Integer corpid, Integer state)
			throws CorpException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("corpId", URLEncoder.encode(corpid.toString(), "UTF-8"));
			params.put("state", URLEncoder.encode(state.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resInteger = Integer.valueOf(result);
			if (resInteger >= 0) {
				return resInteger;
			}
			if (resInteger < 0) {
				throw new CorpException(resInteger, "更改状态失败！！");
			}
		} catch (NumberFormatException e) {
			throw new CorpException("没有返回预期的数据!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
		return -1;
	}

	/**
	 * 添加企业
	 * 
	 * @param ca
	 * @return
	 * @throws CorpAddException
	 */
	public int add(CorpAdd ca) throws CorpAddException {
		try {
			JSONObject json = JSONObject.fromObject(ca);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger >= 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -1:
				throw new CorpAddException(resultInteger, "找不到企业全称字段!");
			case -2:
				throw new CorpAddException(resultInteger, "企业全称不能为空!");
			case -3:
				throw new CorpAddException(resultInteger, "企业全称已经存在!");
			case -4:
				throw new CorpAddException(resultInteger, "找不到企业级别字段!");
			case -5:
				throw new CorpAddException(resultInteger, "企业级别数据有误(<=0)");
			case -6:
				throw new CorpAddException(resultInteger, "找不到对应的企业级别");
			case -7:
				throw new CorpAddException(resultInteger, "找不到行业字段");
			case -8:
				throw new CorpAddException(resultInteger, "行业字段数据有误(<=0)");
			case -9:
				throw new CorpAddException(resultInteger, "找不到到对应的行业");
			case -10:
				throw new CorpAddException(resultInteger, "找不到用户别名字段");
			case -11:
				throw new CorpAddException(resultInteger, "别名不能为空");
			case -12:
				throw new CorpAddException(resultInteger, "找不到用户编号字段");
			case -13:
				throw new CorpAddException(resultInteger, "用户编号有误(<=0)");
			case -14:
				throw new CorpAddException(resultInteger, "找不到IM账号字段");
			case -15:
				throw new CorpAddException(resultInteger, "IM账号不能为空");
			case -16:
				throw new CorpAddException(resultInteger, "指定的创建者");
			case -17:
				throw new CorpAddException(resultInteger, "指定的创建者不能为企业用户");
			case -18:
				throw new CorpAddException(resultInteger, "终止时间必须大于开通时间");
			case -19:
				throw new CorpAddException(resultInteger, "重复的企业代码");
			default:
				throw new CorpAddException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpAddException(e.getMessage(), e);
		}
	}

	/**
	 * 判断企业名称是否存在
	 * 
	 * @param corpName
	 *            企业名称
	 * @return
	 * @throws isCorpExistsException
	 */
	public boolean isCorpExists(String corpName) throws isCorpExistsException {
		try {
			String rs = "";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("corpName", corpName);
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			int code = Integer.parseInt(rs);
			if (code > 0)
				return true;
			switch (code) {
			case 0:
				return false;
			case -1:
				throw new isCorpExistsException(code, "服务器处理异常!");
			case -2:
				throw new isCorpExistsException(code, "企业名称不能为空!");
			default:
				throw new isCorpExistsException(code, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new isCorpExistsException("没有返回预期的数据!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 获取企业分类列表
	 * 
	 * @return
	 */
	public String getCorpCategiries() throws ServiceException {
		try {
			String rs = "";
			Map<String, Object> params = new HashMap<String, Object>();
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			return rs;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 根据企业类别id获取该企业的级别
	 * 
	 * @param categirid
	 * @return
	 */
	public String getCorpLevelsByCateId(Integer cateId) throws CorpException {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cateId", URLEncoder.encode(cateId.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 获取系统行业列表
	 * 
	 * @return
	 */
	public String getIndustries() throws ServiceException {
		try {
			String rs = "";
			Map<String, Object> params = new HashMap<String, Object>();
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			return rs;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 删除企业全部信息，根据企业id(集合形式)
	 * 
	 * @param cd
	 * @return
	 * @throws CorpException
	 */
	public Integer del(CorpDelete cd) throws CorpException {
		try {
			JSONObject json = JSONObject.fromObject(cd);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger >= 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -1:
				throw new CorpException(resultInteger,
						"删除企业失败(可能原因：该企业不存在或是没找到对应的企业)");
			default:
				throw new CorpException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	/**
	 * 更新企业信息
	 * 
	 * @param cu
	 * @return
	 * @throws CorpException
	 */
	public Integer update(CorpUpdate cu) throws CorpException {
		try {
			JSONObject json = JSONObject.fromObject(cu);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case 0:
				throw new CorpException(resultInteger, "参数有误!"
						+ json.toString());
			case -1:
				throw new CorpException(resultInteger, "找不到企业编号字段[id]");
			case -2:
				throw new CorpException(resultInteger, "企业编号有误[<=0]");
			case -3:
				throw new CorpException(resultInteger, "找不到对应的企业!");
			case -4:
				throw new CorpException(resultInteger, "企业全称已被注册!");
			case -5:
				throw new CorpException(resultInteger, "企业级别编号无效[<=0]");
			case -6:
				throw new CorpException(resultInteger, "找不到对应的企业级别");
			case -7:
				throw new CorpException(resultInteger, "行业编号无效[<=0]");
			case -8:
				throw new CorpException(resultInteger, "找不到对应的行业");
			case -9:
				throw new CorpException(resultInteger, "终止时间必须大于等于开通时间");
			case -10:
				throw new CorpException(resultInteger, "企业代码对应的企业不存在");
			default:
				throw new CorpException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	public boolean changeSet(CorpSet set) throws CorpException {
		try {
			JSONObject json = JSONObject.fromObject(set);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}
			switch (resultInteger) {
			case 0:
				throw new CorpException(resultInteger, "参数有误!"
						+ json.toString());
			case -1:
				throw new CorpException(resultInteger, "找不到企业编号字段[id]");
			case -2:
				throw new CorpException(resultInteger, "企业编号有误[<=0]");
			case -3:
				throw new CorpException(resultInteger, "找不到对应的企业!");
			case -4:
				throw new CorpException(resultInteger, "企业代码对应的企业不存在");
			default:
				throw new CorpException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}
	
	public void offline(String ... codes) throws CorpException {
		try {
			JSONArray json = JSONArray.fromObject(codes);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	public CorpSet getSet(String corpCode) throws CorpException {
		JSONObject json = new JSONObject();
		json.put("corpCode", corpCode);
		return getSet(json);
	}

	public CorpSet getSet(Integer corpId) throws CorpException {
		JSONObject json = new JSONObject();
		json.put("corpId", corpId);
		return getSet(json);
	}

	public CorpSet getSet(JSONObject json) throws CorpException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			JSONObject r = JSONObject.fromObject(result);
			if (r.has("success")) {
				if (r.getBoolean("success")) {
					return (CorpSet) JSONObject.toBean(r
							.getJSONObject("result"), CorpSet.class);
				} else {
					Integer resultInteger = r.getInt("error");
					switch (resultInteger) {
					case 0:
						throw new CorpException(resultInteger, "参数有误!"
								+ json.toString());
					case -1:
						throw new CorpException(resultInteger, "找不到企业编号字段[id]");
					case -2:
						throw new CorpException(resultInteger, "企业编号有误[<=0]");
					case -3:
						throw new CorpException(resultInteger, "找不到对应的企业!");
					case -4:
						throw new CorpException(resultInteger, "企业代码对应的企业不存在");
					default:
						throw new CorpException(resultInteger, OTHER_ERROR);
					}
				}
			}
			throw new CorpException(-100, OTHER_ERROR);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	public boolean corpLiang(CorpLiang corpLiang) throws CorpException {
		try {
			JSONObject json = JSONObject.fromObject(corpLiang);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}
			switch (resultInteger) {
			case 0:
				throw new CorpException(resultInteger, "参数有误!"
						+ json.toString());
			case -1:
				throw new CorpException(resultInteger, "找不到企业编号字段[id]");
			case -2:
				throw new CorpException(resultInteger, "企业编号有误[<=0]");
			case -3:
				throw new CorpException(resultInteger, "找不到对应的企业!");
			case -4:
				throw new CorpException(resultInteger, "企业代码对应的企业不存在[corpCode]");
			case -5:
				throw new CorpException(resultInteger, "找不到靓号字段[liang]!");
			case -6:
				throw new CorpException(resultInteger, "找不到账号字段[account]!");
			default:
				throw new CorpException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	public boolean corpLiangBecome(CorpLiang corpLiang) throws CorpException {
		try {
			JSONObject json = JSONObject.fromObject(corpLiang);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}
			switch (resultInteger) {
			case 0:
				throw new CorpException(resultInteger, "参数有误!"
						+ json.toString());
			case -1:
				throw new CorpException(resultInteger, "找不到企业编号字段[id]");
			case -2:
				throw new CorpException(resultInteger, "企业编号有误[<=0]");
			case -3:
				throw new CorpException(resultInteger, "找不到对应的企业!");
			case -4:
				throw new CorpException(resultInteger, "企业代码对应的企业不存在[corpCode]");
			default:
				throw new CorpException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	public boolean guideComplete(String corpCode) throws CorpException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", corpCode);
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}
			switch (resultInteger) {
			case 0:
				throw new CorpException(resultInteger, "处理失败!");
			default:
				throw new CorpException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpException(e.getMessage(), e);
		}
	}

	public String getCorpInfo(CorpInfo corpInfo) throws ServiceException {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(corpInfo);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}
}
