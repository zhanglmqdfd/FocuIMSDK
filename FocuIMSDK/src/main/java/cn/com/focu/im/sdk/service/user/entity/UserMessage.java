package cn.com.focu.im.sdk.service.user.entity;

public class UserMessage {

	private String userLoginName;
	private String friendLoginName;
	private Integer flag;
	private String userLoginNumber;
	private String friendLoginNumber;
	private String content;

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}

	public String getFriendLoginName() {
		return friendLoginName;
	}

	public void setFriendLoginName(String friendLoginName) {
		this.friendLoginName = friendLoginName;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getUserLoginNumber() {
		return userLoginNumber;
	}

	public void setUserLoginNumber(String userLoginNumber) {
		this.userLoginNumber = userLoginNumber;
	}

	public String getFriendLoginNumber() {
		return friendLoginNumber;
	}

	public void setFriendLoginNumber(String friendLoginNumber) {
		this.friendLoginNumber = friendLoginNumber;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
