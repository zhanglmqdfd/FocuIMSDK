package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

@SuppressWarnings("serial")
public class UserCardException extends ServiceException {

	public UserCardException(int code, String message) {
		super(code, message);
	}

	public UserCardException(String message, Throwable cause) {
		super(message, cause);
	}
}
