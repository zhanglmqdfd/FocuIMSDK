package cn.com.focu.im.sdk.service.user.entity;

public class UserFriendAdd {
	
	private Integer flag;
	/**
	 * @return the flag
	 */
	public Integer getFlag() {
		return flag;
	}
	/**
	 * @param flag the flag to set
	 */
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	/**
	 * @return the uid
	 */
	public Integer getUid() {
		return uid;
	}
	/**
	 * @param uid the uid to set
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	/**
	 * @return the fid
	 */
	public Integer getFid() {
		return fid;
	}
	/**
	 * @param fid the fid to set
	 */
	public void setFid(Integer fid) {
		this.fid = fid;
	}
	/**
	 * @return the r
	 */
	public Integer getR() {
		return r;
	}
	/**
	 * @param r the r to set
	 */
	public void setR(Integer r) {
		this.r = r;
	}
	/**
	 * @return the sgid
	 */
	public Integer getSgid() {
		return sgid;
	}
	/**
	 * @param sgid the sgid to set
	 */
	public void setSgid(Integer sgid) {
		this.sgid = sgid;
	}
	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}
	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}
	private Integer uid;
	private Integer fid;
	private Integer r;
	private Integer sgid;
	
	private String loginName;
	private String  fname;

}
