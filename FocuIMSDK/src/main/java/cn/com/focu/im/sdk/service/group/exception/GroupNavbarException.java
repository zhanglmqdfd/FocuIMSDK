package cn.com.focu.im.sdk.service.group.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class GroupNavbarException extends ServiceException {

	private static final long serialVersionUID = -7269927939681931682L;

	public GroupNavbarException(int code, String message) {
		super(code, message);
	}

	public GroupNavbarException(String message, Throwable cause) {
		super(message, cause);
	}
}
