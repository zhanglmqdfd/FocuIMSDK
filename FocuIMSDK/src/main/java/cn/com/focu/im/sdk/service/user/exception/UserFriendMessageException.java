package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserFriendMessageException extends ServiceException {

	private static final long serialVersionUID = -6852854970090895307L;

	public UserFriendMessageException(int code, String message) {
		super(code, message);
	}

	public UserFriendMessageException(String message, Throwable cause) {
		super(message, cause);
	}
}
