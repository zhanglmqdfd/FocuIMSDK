package cn.com.focu.im.sdk.service.user.entity;

public class UserEmailExists {
	private Integer uid;
	private String email;

	public UserEmailExists(String email) {
		super();
		this.email = email;
	}
	
	public UserEmailExists(Integer uid, String email) {
		super();
		this.uid = uid;
		this.email = email;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
