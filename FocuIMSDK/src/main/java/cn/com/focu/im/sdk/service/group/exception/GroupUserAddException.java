package cn.com.focu.im.sdk.service.group.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class GroupUserAddException extends ServiceException {
	private static final long serialVersionUID = 2977823781867397377L;

	public GroupUserAddException(int code, String message) {
		super(code, message);
	}

	public GroupUserAddException(String message, Throwable cause) {
		super(message, cause);
	}
}
