package cn.com.focu.im.sdk.service.user.entity;

public class UserLoginNumberExists {
	
	private Integer uid;
	private String loginNumber;
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getLoginNumber() {
		return loginNumber;
	}
	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}
	public UserLoginNumberExists(Integer uid, String loginNumber) {
		super();
		this.uid = uid;
		this.loginNumber = loginNumber;
	}
	public UserLoginNumberExists(String loginNumber) {
		super();
		this.loginNumber = loginNumber;
	}
}