package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserRestoreException extends ServiceException {

	private static final long serialVersionUID = 8198616021600594553L;

	public UserRestoreException(int code, String message) {
		super(code, message);
	}

	public UserRestoreException(String message, Throwable cause) {
		super(message, cause);
	}
}
