package cn.com.focu.im.sdk.service.apps.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class SyncEventException extends ServiceException {

	private static final long serialVersionUID = 255475831696949203L;
	
	public SyncEventException(int code, String message) {
		super(code, message);
	}

	public SyncEventException(String message, Throwable cause) {
		super(message, cause);
	}
}
