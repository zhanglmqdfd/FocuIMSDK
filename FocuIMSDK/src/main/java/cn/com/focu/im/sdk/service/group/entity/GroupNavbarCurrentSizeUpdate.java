package cn.com.focu.im.sdk.service.group.entity;

public class GroupNavbarCurrentSizeUpdate {
	
	private Integer online;
	private Integer currentSize;
	private Integer navbarId;
	
	public Integer getNavbarId() {
		return navbarId;
	}
	public void setNavbarId(Integer navbarId) {
		this.navbarId = navbarId;
	}
	public Integer getOnline() {
		return online;
	}
	public void setOnline(Integer online) {
		this.online = online;
	}
	public Integer getCurrentSize() {
		return currentSize;
	}
	public void setCurrentSize(Integer currentSize) {
		this.currentSize = currentSize;
	}
}
