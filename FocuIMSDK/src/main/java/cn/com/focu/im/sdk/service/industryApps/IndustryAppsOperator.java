package cn.com.focu.im.sdk.service.industryApps;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.group.exception.GroupNavbarException;
import cn.com.focu.im.sdk.service.industryApps.entity.AddIndustryApps;
import cn.com.focu.im.sdk.service.industryApps.entity.DelIndustryApps;
import cn.com.focu.im.sdk.service.industryApps.entity.IndustryApps;
import cn.com.focu.im.sdk.service.industryApps.entity.UpdateIndustryApps;
import cn.com.focu.im.sdk.service.industryApps.exception.IndustryAppsException;

@Operator("industryApps")
public class IndustryAppsOperator extends ServiceOperator {

	public IndustryAppsOperator() {
	}

	public static class IndustryAppsOperatorHolder {
		static IndustryAppsOperator go = new IndustryAppsOperator();
	}

	public static IndustryAppsOperator getInstence() {
		return IndustryAppsOperatorHolder.go;
	}

	public Integer addIndustryApps(AddIndustryApps addIndustryApps) {
		try {
			JSONObject json = JSONObject.fromObject(addIndustryApps);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger, "提交数据格式错误");
			default:
				throw new IndustryAppsException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new IndustryAppsException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new IndustryAppsException(e.getMessage(), e);
		}
	}
	
	public Integer updateIndustryApps(UpdateIndustryApps updateIndustryApps){
		try {
			JSONObject json = JSONObject.fromObject(updateIndustryApps);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new IndustryAppsException(resutlInteger, "提交数据格式错误");
			case -1:
				throw new IndustryAppsException(resutlInteger, "修改的应用不存在");
			default:
				throw new IndustryAppsException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new IndustryAppsException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new IndustryAppsException(e.getMessage(), e);
		}
	}
	
	public Integer delIndustryApps(DelIndustryApps delIndustryApps){
		try {
			JSONObject json = JSONObject.fromObject(delIndustryApps);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new IndustryAppsException(resutlInteger, "提交数据格式错误");
			case -1:
				throw new IndustryAppsException(resutlInteger, "删除的应用不存在");
			default:
				throw new IndustryAppsException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new IndustryAppsException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new IndustryAppsException(e.getMessage(), e);
		}
	}
	
	public String getIndustryApps(IndustryApps app){
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(app);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}
}
