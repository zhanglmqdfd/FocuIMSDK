package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class RegulationBranchsException extends ServiceException {

	private static final long serialVersionUID = 3163202431312511347L;

	public RegulationBranchsException(int code, String message) {
		super(code, message);
	}

	public RegulationBranchsException(String message, Throwable cause) {
		super(message, cause);
	}
}
