package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserFriendGetException extends ServiceException {

	private static final long serialVersionUID = -4456747111623853364L;

	public UserFriendGetException(int code, String message) {
		super(code, message);
	}

	public UserFriendGetException(String message, Throwable cause) {
		super(message, cause);
	}
}
