package cn.com.focu.im.sdk.service.corp.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class CorpException extends ServiceException {
	private static final long serialVersionUID = -5059600808934949607L;

	public CorpException(int code, String message) {
		super(code, message);
	}

	public CorpException(String message, Throwable cause) {
		super(message, cause);
	}
}
