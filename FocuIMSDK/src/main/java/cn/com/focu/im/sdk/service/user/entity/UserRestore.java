package cn.com.focu.im.sdk.service.user.entity;

public class UserRestore {

	private Integer flag = 1;

	private String loginNumber;

	private Integer uid;

	public UserRestore(String loginNumber) {
		super();
		this.loginNumber = loginNumber;
		this.flag = 2;
	}

	public UserRestore(Integer uid) {
		super();
		this.uid = uid;
		this.flag = 1;
	}

	public Integer getFlag() {
		return flag;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public Integer getUid() {
		return uid;
	}
}
