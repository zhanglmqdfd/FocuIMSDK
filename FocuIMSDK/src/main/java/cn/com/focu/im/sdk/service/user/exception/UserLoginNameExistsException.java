package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserLoginNameExistsException  extends ServiceException{

	private static final long serialVersionUID = 3481143652135949111L;

	public UserLoginNameExistsException(int code, String message) {
		super(code, message);
	}

	public UserLoginNameExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
