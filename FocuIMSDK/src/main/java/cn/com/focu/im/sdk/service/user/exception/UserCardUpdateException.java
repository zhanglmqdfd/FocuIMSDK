package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserCardUpdateException extends ServiceException {

	private static final long serialVersionUID = -775051450253495958L;

	public UserCardUpdateException(int code, String message) {
		super(code, message);
	}

	public UserCardUpdateException(String message, Throwable cause) {
		super(message, cause);
	}
}
