package cn.com.focu.im.sdk.service.industryApps.entity;

public class UpdateIndustryApps {
	private Integer id;
	private String industryName;
	private String menuName;
	private String image;
	private Integer signmethod = 0;
	private Integer defaultAuthority = 0;
	private Integer width = 800;//最小宽度
	private Integer height = 600;// 最小高度
	private boolean disk = false;
	private boolean browserIndependent = true;// 是否使用独立浏览器打开
	private Integer industryType = 0;// 行业应用类型 0：B/S 1：C/S
	private String url;// B/S 表示登录地址 C/S 表示软件下载地址
	private String additional = "";// 附加字符串
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIndustryName() {
		return industryName;
	}
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Integer getSignmethod() {
		return signmethod;
	}
	public void setSignmethod(Integer signmethod) {
		this.signmethod = signmethod;
	}
	public Integer getDefaultAuthority() {
		return defaultAuthority;
	}
	public void setDefaultAuthority(Integer defaultAuthority) {
		this.defaultAuthority = defaultAuthority;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public boolean isDisk() {
		return disk;
	}
	public void setDisk(boolean disk) {
		this.disk = disk;
	}
	public boolean isBrowserIndependent() {
		return browserIndependent;
	}
	public void setBrowserIndependent(boolean browserIndependent) {
		this.browserIndependent = browserIndependent;
	}
	public Integer getIndustryType() {
		return industryType;
	}
	public void setIndustryType(Integer industryType) {
		this.industryType = industryType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAdditional() {
		return additional;
	}
	public void setAdditional(String additional) {
		this.additional = additional;
	}
}
