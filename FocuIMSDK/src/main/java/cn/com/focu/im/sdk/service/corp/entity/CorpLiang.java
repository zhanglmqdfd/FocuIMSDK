package cn.com.focu.im.sdk.service.corp.entity;

public class CorpLiang {
	private String corpCode;
	private Integer corpId;
	private String account;
	private String liang;

	public CorpLiang(String corpCode, String account) {
		super();
		this.corpCode = corpCode;
		this.account = account;
	}
	
	public CorpLiang(Integer corpId, String account) {
		super();
		this.corpId = corpId;
		this.account = account;
	}

	public CorpLiang(String corpCode, String account,
			String liang) {
		super();
		this.corpCode = corpCode;
		this.account = account;
		this.liang = liang;
	}
	
	public CorpLiang(Integer corpId, String account, String liang) {
		super();
		this.corpId = corpId;
		this.account = account;
		this.liang = liang;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public String getAccount() {
		return account;
	}

	public String getLiang() {
		return liang;
	}
}
