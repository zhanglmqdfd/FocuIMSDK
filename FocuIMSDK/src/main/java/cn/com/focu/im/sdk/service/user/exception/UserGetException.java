package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserGetException  extends ServiceException{

	private static final long serialVersionUID = -6368598090818696817L;

	public UserGetException(int code, String message) {
		super(code, message);
	}

	public UserGetException(String message, Throwable cause) {
		super(message, cause);
	}
}
