package cn.com.focu.im.sdk.service.disk.entity;

public class NewDirectory {

	private Integer createId;
	private Integer parentDir;

	public Integer getCreateId() {
		return createId;
	}

	public void setCreateId(Integer createId) {
		this.createId = createId;
	}

	public Integer getParentDir() {
		return parentDir;
	}

	public void setParentDir(Integer parentDir) {
		this.parentDir = parentDir;
	}

}
