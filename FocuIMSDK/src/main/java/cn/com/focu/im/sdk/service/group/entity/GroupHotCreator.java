package cn.com.focu.im.sdk.service.group.entity;

public class GroupHotCreator {

	private Integer[] userId;
	private Integer[] ids;
	private Integer pageNo;
	private Integer pageSize;
	private String keyWord;
	
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	public Integer[] getIds() {
		return ids;
	}
	public void setIds(Integer[] ids) {
		this.ids = ids;
	}
	public Integer[] getUserId() {
		return userId;
	}
	public void setUserId(Integer[] userId) {
		this.userId = userId;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
