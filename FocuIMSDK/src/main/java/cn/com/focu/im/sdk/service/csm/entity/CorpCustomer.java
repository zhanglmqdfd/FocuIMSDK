package cn.com.focu.im.sdk.service.csm.entity;

public class CorpCustomer {
	private String corpCode;
	private String[] loginNumbers;
	
	public String getCorpCode() {
		return corpCode;
	}
	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}
	public String[] getLoginNumbers() {
		return loginNumbers;
	}
	public void setLoginNumbers(String[] loginNumbers) {
		this.loginNumbers = loginNumbers;
	}
	
	
}
