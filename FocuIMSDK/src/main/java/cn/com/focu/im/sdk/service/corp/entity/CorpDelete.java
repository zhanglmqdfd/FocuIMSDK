package cn.com.focu.im.sdk.service.corp.entity;

public class CorpDelete {
	
	/**
	 * @return the corpId
	 */
	public Integer[] getCorpId() {
		return corpId;
	}

	/**
	 * @param corpId the corpId to set
	 */
	public void setCorpId(Integer[] corpId) {
		this.corpId = corpId;
	}

	private Integer [] corpId;
	
}
