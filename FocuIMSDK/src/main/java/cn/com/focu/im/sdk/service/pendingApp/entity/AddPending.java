package cn.com.focu.im.sdk.service.pendingApp.entity;

public class AddPending {
	/**
	 * 待办应用编号
	 */
	private Integer appId;
	/**
	 * 安全令牌
	 */
	private String secureKey;
	/**
	 * 待办主题
	 */
	private String subject;
	/**
	 * 待办内容
	 */
	private String content;
	/**
	 * 用户编号
	 */
	private Integer uid;
	/**
	 * 别名
	 */
	private String loginName;
	/**
	 * IM帐号
	 */
	private String loginNumber;
	/**
	 * 0别名,1 编号,2 数字帐号
	 */
	private Integer flag;
	/**
	 * 应用待办详情页单点登录地址
	 */
	private String toAppUrl;
	/**
	 * 待办状态自动更改为完成，不需要应用系统调用接口更改状态。
	 */
	private Boolean autoComplete;

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getSecureKey() {
		return secureKey;
	}

	public void setSecureKey(String secureKey) {
		this.secureKey = secureKey;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getToAppUrl() {
		return toAppUrl;
	}

	public void setToAppUrl(String toAppUrl) {
		this.toAppUrl = toAppUrl;
	}

	public Boolean getAutoComplete() {
		return autoComplete;
	}

	public void setAutoComplete(Boolean autoComplete) {
		this.autoComplete = autoComplete;
	}

}
