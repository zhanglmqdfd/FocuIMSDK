package cn.com.focu.im.sdk.service.disk.entity;

public class DeleteDirectory {

	private Integer dirId;
	private Integer currentId;
	
	public Integer getDirId() {
		return dirId;
	}
	public void setDirId(Integer dirId) {
		this.dirId = dirId;
	}
	public Integer getCurrentId() {
		return currentId;
	}
	public void setCurrentId(Integer currentId) {
		this.currentId = currentId;
	}
	
}
