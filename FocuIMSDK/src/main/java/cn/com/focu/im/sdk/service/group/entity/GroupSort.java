package cn.com.focu.im.sdk.service.group.entity;

public class GroupSort {
	
	private Integer mGroupId;
	
	private Integer yGroupId;
	
	private Integer orientation;//移动方向0：置顶 、1：向上或向下 、2：置末
	
	private Integer typeId;
	
	private Integer bz;

	public Integer getBz() {
		return bz;
	}

	public void setBz(Integer bz) {
		this.bz = bz;
	}

	public Integer getmGroupId() {
		return mGroupId;
	}

	public void setmGroupId(Integer mGroupId) {
		this.mGroupId = mGroupId;
	}

	public Integer getyGroupId() {
		return yGroupId;
	}

	public void setyGroupId(Integer yGroupId) {
		this.yGroupId = yGroupId;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	
}
