package cn.com.focu.im.sdk.service.user.entity;

public class UserCardDelete {

	private Integer tid;   //卡片类型id
	private Integer gid;   //如果卡片类型id为群名片，则，该字段为组id
	private Integer uid;  //用户id,可以是集合
	
	
	


	/**
	 * @return the uid
	 */
	public Integer getUid() {
		return uid;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
	 * @return the tid
	 */
	public Integer getTid() {
		return tid;
	}

	/**
	 * @param tid the tid to set
	 */
	public void setTid(Integer tid) {
		this.tid = tid;
	}

	/**
	 * @return the gid
	 */
	public Integer getGid() {
		return gid;
	}

	/**
	 * @param gid the gid to set
	 */
	public void setGid(Integer gid) {
		this.gid = gid;
	}


	

}
