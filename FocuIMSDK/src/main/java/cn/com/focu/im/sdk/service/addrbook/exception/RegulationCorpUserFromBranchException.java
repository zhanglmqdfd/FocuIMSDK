package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class RegulationCorpUserFromBranchException extends ServiceException {

	private static final long serialVersionUID = 148165832696701520L;

	public RegulationCorpUserFromBranchException(int code, String message) {
		super(code, message);
	}

	public RegulationCorpUserFromBranchException(String message, Throwable cause) {
		super(message, cause);
	}
}
