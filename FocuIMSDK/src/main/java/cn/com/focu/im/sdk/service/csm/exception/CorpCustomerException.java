package cn.com.focu.im.sdk.service.csm.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class CorpCustomerException extends ServiceException {

	private static final long serialVersionUID = -5023444933382667361L;
	
	public CorpCustomerException(int code,String message){
		super(code,message);
	}
	
	public CorpCustomerException(String message, Throwable cause) {
		super(message, cause);
	}
}
