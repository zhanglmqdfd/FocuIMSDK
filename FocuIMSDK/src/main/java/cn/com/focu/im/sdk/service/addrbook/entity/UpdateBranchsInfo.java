package cn.com.focu.im.sdk.service.addrbook.entity;

public class UpdateBranchsInfo {
	private Integer flag = 2;
	private String corpCode;
	private String bcode;
	private String bname;
	private String sname;
	private Integer corpId;
	
	public UpdateBranchsInfo(){
		
	}

	public UpdateBranchsInfo(String corpCode, String bcode, String bname,
			String sname) {
		super();
		this.corpCode = corpCode;
		this.bcode = bcode;
		this.bname = bname;
		this.sname = sname;
	}

	public UpdateBranchsInfo(String corpCode, String bcode, String bname) {
		super();
		this.corpCode = corpCode;
		this.bcode = bcode;
		this.bname = bname;
		this.sname = bname;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public void setCorpId(Integer corpId) {
		this.corpId = corpId;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getBcode() {
		return bcode;
	}

	public void setBcode(String bcode) {
		this.bcode = bcode;
	}

	public String getBname() {
		return bname;
	}

	public void setBname(String bname) {
		this.bname = bname;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
}
