package cn.com.focu.im.sdk.service.addrbook.entity;

public class AddBranchsToCorp {
	
	private Integer corpId;
	
	private String corpCode;
	
	private String bcode;
	
	private String bname;
	
	private String pcode;
	
	public AddBranchsToCorp(Integer corpId, String bcode, String bname,
			String pcode) {
		super();
		this.corpId = corpId;
		this.bcode = bcode;
		this.bname = bname;
		this.pcode = pcode;
	}

	public AddBranchsToCorp(String corpCode, String bcode, String bname,
			String pcode) {
		super();
		this.corpCode = corpCode;
		this.bcode = bcode;
		this.bname = bname;
		this.pcode = pcode;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public void setCorpId(Integer corpId) {
		this.corpId = corpId;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getBcode() {
		return bcode;
	}

	public void setBcode(String bcode) {
		this.bcode = bcode;
	}

	public String getBname() {
		return bname;
	}

	public void setBname(String bname) {
		this.bname = bname;
	}

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}
}
