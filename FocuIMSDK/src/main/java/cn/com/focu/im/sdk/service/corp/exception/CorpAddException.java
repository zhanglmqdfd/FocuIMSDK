package cn.com.focu.im.sdk.service.corp.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class CorpAddException extends ServiceException {
	private static final long serialVersionUID = -2871633144221193305L;

	public CorpAddException(int code, String message) {
		super(code, message);
	}

	public CorpAddException(String message, Throwable cause) {
		super(message, cause);
	}
}
