package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserStateGetException extends ServiceException {

	private static final long serialVersionUID = -2157944629532195404L;

	public UserStateGetException(int code, String message) {
		super(code, message);
	}

	public UserStateGetException(String message, Throwable cause) {
		super(message, cause);
	}
}
