package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UpdateBranchInfoException extends ServiceException {

	private static final long serialVersionUID = 7838876829852067719L;

	public UpdateBranchInfoException(int code, String message) {
		super(code, message);
	}

	public UpdateBranchInfoException(String message, Throwable cause) {
		super(message, cause);
	}
}
