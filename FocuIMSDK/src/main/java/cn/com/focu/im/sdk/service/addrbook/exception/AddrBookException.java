package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class AddrBookException extends ServiceException {
	private static final long serialVersionUID = -8248982494853477757L;

	public AddrBookException(int code, String message) {
		super(code, message);
	}

	public AddrBookException(String message, Throwable cause) {
		super(message, cause);
	}

	
}
