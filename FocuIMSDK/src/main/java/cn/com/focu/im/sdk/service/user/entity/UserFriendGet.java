package cn.com.focu.im.sdk.service.user.entity;

public class UserFriendGet {
	
	private Integer flag;
	private String loginName;
	private Integer uid;
	private Integer sgid;
	private Integer statleFlag = 0;
	
	public Integer getStatleFlag() {
		return statleFlag;
	}
	public void setStatleFlag(Integer statleFlag) {
		this.statleFlag = statleFlag;
	}
	/**
	 * @return the flag
	 */
	public Integer getFlag() {
		return flag;
	}
	/**
	 * @param flag the flag to set
	 */
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return the uid
	 */
	public Integer getUid() {
		return uid;
	}
	/**
	 * @param uid the uid to set
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	/**
	 * @return the sgid
	 */
	public Integer getSgid() {
		return sgid;
	}
	/**
	 * @param sgid the sgid to set
	 */
	public void setSgid(Integer sgid) {
		this.sgid = sgid;
	}

}
