package cn.com.focu.im.sdk.service.apps;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.apps.entity.AppOrder;
import cn.com.focu.im.sdk.service.apps.exception.AppOrderException;
import cn.com.focu.im.sdk.service.apps.exception.SyncEventException;
import cn.com.focu.im.sdk.service.corp.exception.CorpException;
import cn.com.focu.im.sdk.service.exception.ServiceException;

@Operator("app")
public class AppOperator extends ServiceOperator {

	private AppOperator() {

	}

	public static class SysAppOperatorHolder {
		static AppOperator go = new AppOperator();
	}

	public static AppOperator getInstence() {
		return SysAppOperatorHolder.go;
	}

	public void syncEvents(Boolean corp, String loginName, Integer appId,
			Integer events) throws SyncEventException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("loginName", URLEncoder.encode(loginName.toString(),
					"UTF-8"));
			params.put("appId", URLEncoder.encode(appId.toString(), "UTF-8"));
			params.put("events", URLEncoder.encode(events.toString(), "UTF-8"));
			params.put("corp", URLEncoder.encode(corp.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			JSONObject r = JSONObject.fromObject(result);
			boolean success = r.getBoolean("success");
			if (!success) {
				int error = r.getInt("error");
				String[] errorMessage = new String[] { "无效的用户名!", "应用编号格式有误!",
						"找不到对应的应用!", "应用无待办事件!" };
				throw new SyncEventException(error, errorMessage[error - 1]);
			}
		} catch (JSONException e) {
			throw new CorpException("没有返回预期的数据!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SyncEventException(e.getMessage(), e);
		}
	}

	public void syncEvents2(Boolean corp, Integer appId, String jaArray,
			Boolean store) throws SyncEventException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonArray", URLEncoder.encode(jaArray.toString(),
					"UTF-8"));
			params.put("appId", URLEncoder.encode(appId.toString(), "UTF-8"));
			params.put("corp", URLEncoder.encode(corp.toString(), "UTF-8"));
			params.put("store", URLEncoder.encode(store.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			JSONObject r = JSONObject.fromObject(result);
			boolean success = r.getBoolean("success");
			if (!success) {
				int error = r.getInt("error");
				String[] errorMessage = new String[] { "无效的用户名!", "应用编号格式有误!",
						"找不到对应的应用!", "应用无待办事件!" };
				throw new SyncEventException(error, errorMessage[error - 1]);
			}
		} catch (JSONException e) {
			throw new CorpException("没有返回预期的数据!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SyncEventException(e.getMessage(), e);
		}
	}

	public Integer deleteEventsByAppId(Boolean corp, Integer appId)
			throws SyncEventException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("corp", URLEncoder.encode(corp.toString(), "UTF-8"));
			params.put("appId", URLEncoder.encode(appId.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new SyncEventException(resutlInteger, "数据提交失败，提交数据格式错误");
			case -1:
				throw new SyncEventException(resutlInteger, "应用不存在");
			default:
				throw new SyncEventException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new SyncEventException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SyncEventException(e.getMessage(), e);
		}
	}

	public void order(AppOrder order) throws SyncEventException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder.encode(JSONObject.fromObject(order).toString(),
					"UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			JSONObject r = JSONObject.fromObject(re);
			if (r.has("success") && r.getBoolean("success")) {
				return;
			}
			throw new AppOrderException(r.getInt("error"), r.getString("msg"));
		} catch (NumberFormatException e) {
			throw new AppOrderException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new AppOrderException(e.getMessage(), e);
		}
	}

}
