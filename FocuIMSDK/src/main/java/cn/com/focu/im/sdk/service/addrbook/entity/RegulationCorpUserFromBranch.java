package cn.com.focu.im.sdk.service.addrbook.entity;

public class RegulationCorpUserFromBranch {
	private Integer flag = 0;
	private Integer corpId;
	private String bcode;
	private String corpCode;

	private String[] loginName;

	private Integer[] uid;

	private String[] loginNumber;

	public RegulationCorpUserFromBranch(Integer corpId, String bcode) {
		super();
		this.corpId = corpId;
		this.bcode = bcode;
	}

	public RegulationCorpUserFromBranch(String corpCode, String bcode) {
		super();
		this.corpCode = corpCode;
		this.bcode = bcode;
	}

	public Integer getFlag() {
		return flag;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public String getBcode() {
		return bcode;
	}

	public void setBcode(String bcode) {
		this.bcode = bcode;
	}

	public String[] getLoginName() {
		return loginName;
	}

	public void setLoginName(String[] loginName) {
		this.flag = 0;
		this.loginName = loginName;
	}

	public Integer[] getUid() {
		return uid;
	}

	public void setUid(Integer[] uid) {
		this.flag = 1;
		this.uid = uid;
	}

	public String[] getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String[] loginNumber) {
		this.flag = 2;
		this.loginNumber = loginNumber;
	}

	public String getCorpCode() {
		return corpCode;
	}
}
