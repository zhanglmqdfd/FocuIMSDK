package cn.com.focu.im.sdk.service.sso;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.sso.entity.SSOAuthirtyLogin;
import cn.com.focu.im.sdk.service.sso.exception.SSOAuthorityException;

@Operator("sso")
public class SSOAuthorityOperator extends ServiceOperator {

	private static class SSOAuthorityOperaotrHolder {
		static SSOAuthorityOperator sao = new SSOAuthorityOperator();
	}

	public static SSOAuthorityOperator getInstance() {
		return SSOAuthorityOperaotrHolder.sao;
	}

	/**
	 * 登录(获得认证)
	 * 
	 * @param sal
	 * @return mainTicket
	 * @throws SSOAuthorityException
	 */
	public String login(SSOAuthirtyLogin sal) throws SSOAuthorityException {
		try {
			JSONObject json = JSONObject.fromObject(sal);
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("appId", URLEncoder.encode(json.getString("appId"),
					"UTF-8"));
			paramsMap.put("loginName", URLEncoder.encode(json
					.getString("loginName"), "UTF-8"));

			String re = postReturnString(paramsMap);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			if (StringUtils.isNotBlank(re)) {
				return re;
			}
		}catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SSOAuthorityException(e.getMessage(),e);
		}
		return null;
	}

	/**
	 * 获取服务令牌
	 * 
	 * @param mainTicket
	 * @return
	 * @throws SSOAuthorityException
	 */
	public String getServiceTicket(String mainTicket)
			throws SSOAuthorityException {
		try {
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("mainTicket", URLEncoder.encode(
					mainTicket.toString(), "UTF-8"));
			String re = postReturnString(paramsMap);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			if (StringUtils.isNotBlank(re)) {
				return re;
			}
		}catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SSOAuthorityException(e.getMessage(),e);
		}
		return null;
	}

	/**
	 * 判断令牌是否有效
	 * 
	 * @param serviceTicket
	 * @return
	 * @throws SSOAuthorityException
	 */
	public String isAuthority(String serviceTicket)
			throws SSOAuthorityException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("clientKey", URLEncoder.encode(serviceTicket.toString(),
					"UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			if (StringUtils.isNotBlank(result)) {
				return result;
			}
		}catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}catch (Exception e) {
			throw new SSOAuthorityException(e.getMessage(),e);
		}
		return "";
	}

}
