package cn.com.focu.im.sdk.service.pendingApp.entity;

public class AddPendingState {
	/**
	 * 待办应用编号
	 */
	private Integer appId;
	/**
	 * 安全令牌
	 */
	private String secureKey;
	/**
	 * 待办编号
	 */
	private Integer pendingId;
	/**
	 * 待办状态
	 */
	private String state;
	/**
	 * 处理完毕，默认false。处理完毕指该应用的当前待办处理完毕。处理完毕的待办不可新增待办状态或删除待办
	 */
	private Boolean complete;

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getSecureKey() {
		return secureKey;
	}

	public void setSecureKey(String secureKey) {
		this.secureKey = secureKey;
	}

	public Integer getPendingId() {
		return pendingId;
	}

	public void setPendingId(Integer pendingId) {
		this.pendingId = pendingId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Boolean getComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

}
