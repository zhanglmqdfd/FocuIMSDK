package cn.com.focu.im.sdk.service.corp.entity;

public class CorpSet {

	private String corpCode;

	private Integer corpId;

	private boolean allowSearch = false;
	
	private boolean allowContact = false;

	private boolean allowChangeDisplayName = true;
	
	private boolean corpUserSort = true;
	
	private boolean miniEnable = false;
	
	private String miniUrl;
	
	private String miniParams;
	
	private Integer miniWidth;
	
	private Integer miniHeight;
	
	public CorpSet() {
		super();
	}
	
	public CorpSet(boolean allowSearch, boolean allowContact,
			boolean allowChangeDisplayName, boolean corpUserSort,boolean miniEnable,String miniUrl,String miniParams,Integer miniWidth,Integer miniHeight){
		super();
		this.allowSearch = allowSearch;
		this.allowContact = allowContact;
		this.allowChangeDisplayName = allowChangeDisplayName;
		this.corpUserSort = corpUserSort;
		this.miniEnable = miniEnable;
		this.miniUrl = miniUrl;
		this.miniParams = miniParams;
		this.miniWidth = miniWidth;
		this.miniHeight = miniHeight;
	}
	
	public CorpSet(String corpCode, boolean allowSearch, boolean allowContact,
			boolean allowChangeDisplayName, boolean corpUserSort,boolean miniEnable,String miniUrl,String miniParams,Integer miniWidth,Integer miniHeight) {
		this(allowSearch, allowContact, allowChangeDisplayName,corpUserSort,miniEnable,miniUrl,miniParams,miniWidth,miniHeight);
		this.corpCode = corpCode;
	}

	public CorpSet(Integer corpId, boolean allowSearch, boolean allowContact,
			boolean allowChangeDisplayName, boolean corpUserSort,boolean miniEnable,String miniUrl,String miniParams,Integer miniWidth,Integer miniHeight) {
		this(allowSearch, allowContact, allowChangeDisplayName,corpUserSort,miniEnable,miniUrl,miniParams,miniWidth,miniHeight);
		this.corpId = corpId;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public void setCorpId(Integer corpId) {
		this.corpId = corpId;
	}

	public boolean isAllowSearch() {
		return allowSearch;
	}

	public void setAllowSearch(boolean allowSearch) {
		this.allowSearch = allowSearch;
	}

	public boolean isAllowContact() {
		return allowContact;
	}

	public void setAllowContact(boolean allowContact) {
		this.allowContact = allowContact;
	}

	public boolean isAllowChangeDisplayName() {
		return allowChangeDisplayName;
	}

	public void setAllowChangeDisplayName(boolean allowChangeDisplayName) {
		this.allowChangeDisplayName = allowChangeDisplayName;
	}

	public boolean isCorpUserSort() {
		return corpUserSort;
	}

	public void setCorpUserSort(boolean corpUserSort) {
		this.corpUserSort = corpUserSort;
	}

	public boolean isMiniEnable() {
		return miniEnable;
	}

	public void setMiniEnable(boolean miniEnable) {
		this.miniEnable = miniEnable;
	}

	public String getMiniUrl() {
		return miniUrl;
	}

	public void setMiniUrl(String miniUrl) {
		this.miniUrl = miniUrl;
	}

	public String getMiniParams() {
		return miniParams;
	}

	public void setMiniParams(String miniParams) {
		this.miniParams = miniParams;
	}

	public Integer getMiniWidth() {
		return miniWidth;
	}

	public void setMiniWidth(Integer miniWidth) {
		this.miniWidth = miniWidth;
	}

	public Integer getMiniHeight() {
		return miniHeight;
	}

	public void setMiniHeight(Integer miniHeight) {
		this.miniHeight = miniHeight;
	}
}
