package cn.com.focu.im.sdk.service.group.entity;

public class GroupNavbarRelationsSort {
	
	private Integer mGroupId;
	
	private Integer yGroupId;
	
	private Integer orientation;//移动方向0：置顶 、1：向上或向下 、2：置末
	
	private Integer navbarId;

	public Integer getNavbarId() {
		return navbarId;
	}

	public void setNavbarId(Integer navbarId) {
		this.navbarId = navbarId;
	}

	public Integer getmGroupId() {
		return mGroupId;
	}

	public void setmGroupId(Integer mGroupId) {
		this.mGroupId = mGroupId;
	}

	public Integer getyGroupId() {
		return yGroupId;
	}

	public void setyGroupId(Integer yGroupId) {
		this.yGroupId = yGroupId;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}
	
	
}
