package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class AddBranchsToCorpException extends ServiceException {

	private static final long serialVersionUID = 234040222475978727L;

	public AddBranchsToCorpException(int code, String message) {
		super(code, message);
	}

	public AddBranchsToCorpException(String message, Throwable cause) {
		super(message, cause);
	}
}
