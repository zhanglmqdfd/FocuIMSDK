package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserFriendAddException extends ServiceException {

	private static final long serialVersionUID = 7552823411135762550L;

	public UserFriendAddException(int code, String message) {
		super(code, message);
	}

	public UserFriendAddException(String message, Throwable cause) {
		super(message, cause);
	}
}
