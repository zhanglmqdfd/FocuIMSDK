package cn.com.focu.im.sdk.service.addrbook.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class SyncToClientException  extends ServiceException{

	private static final long serialVersionUID = 2940387601330582975L;
	
	public SyncToClientException(int code, String message) {
		super(code, message);
	}

	public SyncToClientException(String message, Throwable cause) {
		super(message, cause);
	}
}
