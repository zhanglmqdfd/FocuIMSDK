package cn.com.focu.im.sdk.service.group.entity;

public class GroupHotCreatorSort {

	private Integer mUserId;
	
	private Integer yUserId;
	
	private Integer orientation;

	public Integer getmUserId() {
		return mUserId;
	}

	public void setmUserId(Integer mUserId) {
		this.mUserId = mUserId;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}

	public Integer getyUserId() {
		return yUserId;
	}

	public void setyUserId(Integer yUserId) {
		this.yUserId = yUserId;
	}
	
}
