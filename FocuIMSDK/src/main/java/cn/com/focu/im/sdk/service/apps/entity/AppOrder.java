package cn.com.focu.im.sdk.service.apps.entity;

import java.io.Serializable;

public class AppOrder implements Serializable{
	
	private static final long serialVersionUID = -5812246785270476077L;

	private Integer flag;

	private String loginName;

	private String loginNumber;

	private Integer uid;
	
	private Integer appId;
	
	/**
	 * 是否可用
	 */
	private boolean available = false;

	public Integer getFlag() {
		return flag;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.flag = 0;
		this.loginName = loginName;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.flag = 2;
		this.loginNumber = loginNumber;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.flag = 1;
		this.uid = uid;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}
