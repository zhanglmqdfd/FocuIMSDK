package cn.com.focu.im.sdk.service.sms.entity;

public class SmsEntity {

	/**
	 * 发送者帐号
	 */
	private String sender;
	
	/**
	 * 接收短信手机号
	 */
	private String[] mobiles;
	
	/**
	 * 接收短信用户
	 */
	private String[] receivers;
	
	/**
	 * 是否附加系统默认前后缀
	 */
	private Boolean customize = true;
	
	/**
	 * 短信内容
	 */
	private String content;
	
	/**
	 * 检查余额
	 */
	private Boolean checkBalance = true;
	
	public SmsEntity() {
		super();
	}
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String[] getMobiles() {
		return mobiles;
	}

	public void setMobiles(String[] mobiles) {
		this.mobiles = mobiles;
	}

	public String[] getReceivers() {
		return receivers;
	}

	public void setReceivers(String[] receivers) {
		this.receivers = receivers;
	}

	public Boolean getCustomize() {
		return customize;
	}

	public void setCustomize(Boolean customize) {
		this.customize = customize;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getCheckBalance() {
		return checkBalance;
	}

	public void setCheckBalance(Boolean checkBalance) {
		this.checkBalance = checkBalance;
	}
}
