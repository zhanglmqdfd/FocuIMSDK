package cn.com.focu.im.sdk.service.msg.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class SysMessageException extends ServiceException {

	private static final long serialVersionUID = -3154161229849670055L;

	public SysMessageException(int code, String message) {
		super(code, message);
	}

	public SysMessageException(String message, Throwable cause) {
		super(message, cause);
	}
}
