package cn.com.focu.im.sdk.service.pendingApp.entity;

public class DeletePending {
	/**
	 * 待办应用编号
	 */
	private Integer appId;
	/**
	 * 安全令牌
	 */
	private String secureKey;
	/**
	 * 用户编号
	 */
	private Integer uid;
	/**
	 * 别名
	 */
	private String loginName;
	/**
	 * IM帐号
	 */
	private String loginNumber;
	/**
	 * 0别名,1 编号,2 数字帐号
	 */
	private Integer flag;
	/**
	 * 为true删除指定用户在该应用下的所有待办,默认false
	 */
	private Boolean deleteAll;
	/**
	 * 待办编号
	 */
	private Integer[] pendingId;

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getSecureKey() {
		return secureKey;
	}

	public void setSecureKey(String secureKey) {
		this.secureKey = secureKey;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Boolean getDeleteAll() {
		return deleteAll;
	}

	public void setDeleteAll(Boolean deleteAll) {
		this.deleteAll = deleteAll;
	}

	public Integer[] getPendingId() {
		return pendingId;
	}

	public void setPendingId(Integer[] pendingId) {
		this.pendingId = pendingId;
	}

}
