package cn.com.focu.im.sdk.service.sms;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.sms.entity.SmsEntity;

@Operator("sms")
public class SmsOperator extends ServiceOperator {
	public SmsOperator() {
	}

	public static class SmsOperatorHolder {
		static SmsOperator go = new SmsOperator();
	}

	public static SmsOperator getInstence() {
		return SmsOperatorHolder.go;
	}

	/**
	 * 短信发送接口
	 * 
	 * @param sms
	 *            短信实体
	 * @return
	 */
	public String send(SmsEntity sms) throws ServiceException {
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder.encode(JSONObject.fromObject(
					sms).toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return str;
	}
}
