package cn.com.focu.im.sdk.service.disk.entity;

public class RenameDirectory {

	private Integer dirId;
	private String newName;
	private Integer currentId;
	
	public Integer getDirId() {
		return dirId;
	}
	public void setDirId(Integer dirId) {
		this.dirId = dirId;
	}
	public String getNewName() {
		return newName;
	}
	public void setNewName(String newName) {
		this.newName = newName;
	}
	public Integer getCurrentId() {
		return currentId;
	}
	public void setCurrentId(Integer currentId) {
		this.currentId = currentId;
	}

	
}
