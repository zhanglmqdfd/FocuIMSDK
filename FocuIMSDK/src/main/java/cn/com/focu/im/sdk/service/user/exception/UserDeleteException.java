package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserDeleteException extends ServiceException{

	private static final long serialVersionUID = -8743113879636435240L;

	public UserDeleteException(int code, String message) {
		super(code, message);
	}

	public UserDeleteException(String message, Throwable cause) {
		super(message, cause);
	}
}
