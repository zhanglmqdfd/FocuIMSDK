package cn.com.focu.im.sdk.service.addrbook;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.addrbook.entity.AddBranchsToCorp;
import cn.com.focu.im.sdk.service.addrbook.entity.AddrBookAdd;
import cn.com.focu.im.sdk.service.addrbook.entity.ArrowBranchs;
import cn.com.focu.im.sdk.service.addrbook.entity.ArrowUser;
import cn.com.focu.im.sdk.service.addrbook.entity.BranchUsers;
import cn.com.focu.im.sdk.service.addrbook.entity.Branchs;
import cn.com.focu.im.sdk.service.addrbook.entity.DeleteBranchsFromCorp;
import cn.com.focu.im.sdk.service.addrbook.entity.RegulationBranchs;
import cn.com.focu.im.sdk.service.addrbook.entity.RegulationCorpUserFromBranch;
import cn.com.focu.im.sdk.service.addrbook.entity.SyncToClient;
import cn.com.focu.im.sdk.service.addrbook.entity.UpdateBranchsInfo;
import cn.com.focu.im.sdk.service.addrbook.exception.AddBranchsToCorpException;
import cn.com.focu.im.sdk.service.addrbook.exception.AddrBookException;
import cn.com.focu.im.sdk.service.addrbook.exception.ArrowBranchsException;
import cn.com.focu.im.sdk.service.addrbook.exception.ArrowUserException;
import cn.com.focu.im.sdk.service.addrbook.exception.DeleteBranchsFromCorpException;
import cn.com.focu.im.sdk.service.addrbook.exception.IsBranchsExistsException;
import cn.com.focu.im.sdk.service.addrbook.exception.RegulationBranchsException;
import cn.com.focu.im.sdk.service.addrbook.exception.RegulationCorpUserFromBranchException;
import cn.com.focu.im.sdk.service.addrbook.exception.SyncToClientException;
import cn.com.focu.im.sdk.service.addrbook.exception.UpdateBranchInfoException;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.apps.exception.SyncEventException;
import cn.com.focu.im.sdk.service.corp.exception.CorpException;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;

@Operator("addrbook")
public class AddrBookOperator extends ServiceOperator {

	public AddrBookOperator() {
	}

	private static class AddrBookOperatorHolder {
		static AddrBookOperator ado = new AddrBookOperator();
	}

	public static AddrBookOperator getInstance() {
		return AddrBookOperatorHolder.ado;
	}

	public boolean arrowBranchs(ArrowBranchs arrow)
			throws ArrowBranchsException {
		try {
			JSONObject json = JSONObject.fromObject(arrow);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}
			switch (resultInteger) {
			case 0:
				throw new ArrowBranchsException(resultInteger, "json格式数据错误!");
			case -1:
				throw new ArrowBranchsException(resultInteger,
						"找不到字段[direction]或是方向不属于[U|D|T|B]!");
			case -2:
				throw new ArrowBranchsException(resultInteger,
						"企业代码或编号字段不存在[corpCode|corpId],视flag传递!");
			case -3:
				throw new ArrowBranchsException(resultInteger,
						"企业代码或编号值为null或<=0!");
			case -4:
				throw new ArrowBranchsException(resultInteger,
						"找不到对应企业代码或编号的企业!");
			case -5:
				throw new ArrowBranchsException(resultInteger,
						"找不到要移动的部门代码字段[bcode]!");
			case -6:
				throw new ArrowBranchsException(resultInteger, "要移动的部门代码为空!");
			case -7:
				throw new ArrowBranchsException(resultInteger,
						"找不到对应移动部门代码的部门!");
			case -8:
				throw new ArrowBranchsException(resultInteger,
						"找不到目标部门代码字段[pcode]!");
			case -9:
				throw new ArrowBranchsException(resultInteger, "部门目标代码为空!");
			case -10:
				throw new ArrowBranchsException(resultInteger,
						"部门目标代码对应的部门不存在!");
			default:
				throw new ArrowBranchsException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new ArrowBranchsException(e.getMessage(), e);
		}
	}

	public int arrowUser(ArrowUser arrow) throws ArrowUserException {
		try {
			JSONObject json = JSONObject.fromObject(arrow);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case 0:
				throw new ArrowUserException(resultInteger, "json格式数据错误!");
			case -1:
				throw new ArrowUserException(resultInteger,
						"找不到字段[direction]或是方向不属于[U|D|T|B]!");
			case -2:
				throw new ArrowUserException(resultInteger,
						"找不到要移动的人员数字账号字段[loginNumber]!");
			case -3:
				throw new ArrowUserException(resultInteger, "要移动的人员数字账号为空!");
			case -4:
				throw new ArrowUserException(resultInteger, "找不到对应移动数字账号的人员!");
			case -5:
				throw new ArrowUserException(resultInteger,
						"找不到目标数字账号字段[tloginName]!");
			case -6:
				throw new ArrowUserException(resultInteger, "目标数字账号为空!");
			case -7:
				throw new ArrowUserException(resultInteger, "目标数字账号对应的人员不存在!");
			default:
				throw new ArrowUserException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new ArrowUserException(e.getMessage(), e);
		}
	}

	private boolean syncToClient(SyncToClient syncToClient)
			throws SyncToClientException {
		try {
			JSONObject json = JSONObject.fromObject(syncToClient);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}

			switch (resultInteger) {
			case 0:
				throw new SyncToClientException(resultInteger, "json格式转换错误!");
			case -1:
				throw new SyncToClientException(resultInteger,
						"参数提供错误,无效的json格式!");
			case -2:
				throw new SyncToClientException(resultInteger,
						"指定的企业代码或编号找不到对应的企业!");
			default:
				throw new SyncToClientException(resultInteger, OTHER_ERROR);
			}
		} catch (ConnectException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SyncToClientException(e.getMessage(), e);
		}
	}

	public boolean syncToClient(Integer corpId) throws SyncToClientException {
		return syncToClient(new SyncToClient(corpId));
	}

	public boolean syncToClient(String corpCode) throws SyncToClientException {
		return syncToClient(new SyncToClient(corpCode));
	}

	public int deleteBranchsFromCorp(DeleteBranchsFromCorp deleteCorp)
			throws DeleteBranchsFromCorpException {
		try {
			JSONObject json = JSONObject.fromObject(deleteCorp);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case 0:
				throw new DeleteBranchsFromCorpException(resultInteger,
						"json格式数据错误!");
			case -1:
				throw new DeleteBranchsFromCorpException(resultInteger,
						"参数提供错误,请检查!");
			case -2:
				throw new DeleteBranchsFromCorpException(resultInteger,
						"企业下没有该部门!");
			case -3:
				throw new DeleteBranchsFromCorpException(resultInteger,
						"该部门下存在人员，请删除人员后操作!");
			case -4:
				throw new DeleteBranchsFromCorpException(resultInteger,
						"该部门下存在子部门，请先删除子部门后操作!");
			default:
				throw new DeleteBranchsFromCorpException(resultInteger,
						OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new DeleteBranchsFromCorpException(e.getMessage(), e);
		}
	}

	public int updateBranchInfo(UpdateBranchsInfo updateBranchsInfo)
			throws UpdateBranchInfoException {
		try {
			JSONObject json = JSONObject.fromObject(updateBranchsInfo);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case 0:
				throw new UpdateBranchInfoException(resultInteger,
						"json格式数据错误!");
			case -1:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[flag]字段");
			case -2:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[corpId]字段");
			case -3:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[bcode]字段");
			case -4:
				throw new UpdateBranchInfoException(resultInteger, "企业下没有该部门!");
			case -5:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[branchId]字段");
			case -6:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[bname]字段");
			case -7:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[sname]字段");
			case -8:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到[corpCode]字段");
			case -9:
				throw new UpdateBranchInfoException(resultInteger,
						"corpCode数据无效,不能为NULL or Empty!");
			case -10:
				throw new UpdateBranchInfoException(resultInteger,
						"找不到对应corpCode的企业!");
			default:
				throw new UpdateBranchInfoException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new UpdateBranchInfoException(e.getMessage(), e);
		}
	}

	/**
	 * 添加人员到部门
	 * 
	 * @return
	 */
	public int joinToBranchs(AddrBookAdd aba) throws AddrBookException {
		try {
			JSONObject json = JSONObject.fromObject(aba);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger >= 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -2:
				throw new AddrBookException(resultInteger, "数据提交到服务器发生异常");
			case -1:
				throw new AddrBookException(resultInteger, "新增的用户已经是企业人员");
			case -3:
				throw new AddrBookException(resultInteger, "要添加用户的部门不存在!");
			case -4:
				throw new AddrBookException(resultInteger, "待添加的用户必须为非企业人员!");
			case -5:
				throw new AddrBookException(resultInteger, "企业代码对应的企业不存在");
			default:
				throw new AddrBookException(resultInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new AddrBookException(e.getMessage(), e);
		}
	}

	public boolean isBranchsExists(String corpCode, String bcode)
			throws IsBranchsExistsException {

		if (StringUtils.isBlank(corpCode))
			throw new IsBranchsExistsException(-4, "必须提供企业代码字段,且不能为NULL!");

		if (StringUtils.isBlank(bcode))
			throw new IsBranchsExistsException(-2,
					"必须提供部门代码字段,且不得为NULL or 空字符!");

		JSONObject json = new JSONObject();
		json.put("corpCode", corpCode);
		json.put("bcode", bcode);
		return isBranchsExists(json);
	}

	private boolean isBranchsExists(JSONObject json)
			throws IsBranchsExistsException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return true;
			}

			switch (resultInteger) {
			case 0:
				return false;
			case -1:
				throw new IsBranchsExistsException(resultInteger,
						"参数提供错误,无效的json格式!");
			case -2:
				throw new IsBranchsExistsException(resultInteger, "部门代码字段未提供!");
			case -3:
				throw new IsBranchsExistsException(resultInteger, "部门代码不能为空!");
			case -4:
				throw new IsBranchsExistsException(resultInteger,
						"必须提供企业id或企业代码!");

			case -5:
				throw new IsBranchsExistsException(resultInteger,
						"企业id提供有误,必须>0!");
			case -6:
				throw new IsBranchsExistsException(resultInteger,
						"企业id无效,找不到对应的企业!");
			case -7:
				throw new IsBranchsExistsException(resultInteger,
						"企业代码无效,找不到对应的企业!");
			case -8:
				throw new IsBranchsExistsException(resultInteger,
						"服务器繁忙,请稍候再试!");
			default:
				throw new IsBranchsExistsException(resultInteger, OTHER_ERROR);
			}
		} catch (ConnectException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new IsBranchsExistsException(e.getMessage(), e);
		}
	}

	public boolean isBranchsExists(Integer corpId, String bcode)
			throws IsBranchsExistsException {

		if (corpId == null)
			throw new IsBranchsExistsException(-4, "必须提供企业编号字段,且不能为NULL!");

		if (corpId <= 0)
			throw new IsBranchsExistsException(-5, "企业编号必须>0!");

		if (StringUtils.isBlank(bcode))
			throw new IsBranchsExistsException(-2,
					"必须提供部门代码字段,且不得为NULL or 空字符!");

		JSONObject json = new JSONObject();
		json.put("bcode", bcode);
		json.put("corpId", corpId);
		return isBranchsExists(json);
	}

	public int addBranchsToCorp(AddBranchsToCorp addBranchsToCorp)
			throws AddBranchsToCorpException {
		try {
			JSONObject json = JSONObject.fromObject(addBranchsToCorp);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -1:
				throw new AddBranchsToCorpException(resultInteger,
						"找不到对应的企业编号字段[corpId]!");
			case -2:
				throw new AddBranchsToCorpException(resultInteger,
						"企业编号数据有误[<=0]!");
			case -3:
				throw new AddBranchsToCorpException(resultInteger,
						"找不到对应编号的企业!");
			case -4:
				throw new AddBranchsToCorpException(resultInteger, "父级代码不存在!");
			case -5:
				throw new AddBranchsToCorpException(resultInteger, "找不到部门代码字段!");
			case -6:
				throw new AddBranchsToCorpException(resultInteger, "部门代码已经存在!");
			case -7:
				throw new AddBranchsToCorpException(resultInteger, "找不到部门名称字段!");
			case -8:
				throw new AddBranchsToCorpException(resultInteger, "部门名称重复!");
			case -9:
				throw new AddBranchsToCorpException(resultInteger, "保存部门信息失败!");
			case -10:
				throw new AddBranchsToCorpException(resultInteger,
						"企业代码对应企业不存在!");
			default:
				throw new AddBranchsToCorpException(resultInteger, OTHER_ERROR);
			}
		} catch (ConnectException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new AddBranchsToCorpException(e.getMessage(), e);
		}
	}

	public int regulationBranchs(RegulationBranchs regulationBranchs)
			throws RegulationBranchsException {
		try {
			JSONObject json = JSONObject.fromObject(regulationBranchs);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -1:
				throw new RegulationBranchsException(resultInteger,
						"要变更的部门代码不能为空!");
			case -2:
				throw new RegulationBranchsException(resultInteger,
						"将要调整到的部门代码不能为空!");
			case -3:
				throw new RegulationBranchsException(resultInteger,
						"必须提供要变更部门所在企业的编号!");
			case -4:
				throw new RegulationBranchsException(resultInteger, "指定的企业不存在!");
			case -5:
				throw new RegulationBranchsException(resultInteger,
						"无效的部门代码,可能企业中没有该部门或该部门不属于企业!");
			case -6:
				throw new RegulationBranchsException(resultInteger,
						"无效的上级部门代码,可能企业中没有该部门或该部门不属于传入的企业!");
			case -7:
				throw new RegulationBranchsException(resultInteger,
						"不能将部门挂在到它的子部门下面!");
			case -8:
				throw new RegulationBranchsException(resultInteger,
						regulationBranchs.getPcode() + "已经是"
								+ regulationBranchs.getBcode() + "的上级部门，不需要变更!");
			default:
				throw new RegulationBranchsException(resultInteger, OTHER_ERROR);
			}
		} catch (ConnectException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new RegulationBranchsException(e.getMessage(), e);
		}
	}

	public int regulationCorpUserFromBranch(
			RegulationCorpUserFromBranch regulationCorpUserFromBranch)
			throws RegulationCorpUserFromBranchException {
		try {
			JSONObject json = JSONObject
					.fromObject(regulationCorpUserFromBranch);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));

			String result = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(result);
			}
			Integer resultInteger = Integer.valueOf(result);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case 0:
				throw new RegulationCorpUserFromBranchException(resultInteger,
						"json格式转换有误!");
			case -1:
				throw new RegulationCorpUserFromBranchException(resultInteger,
						"字段bcode不能为空!");
			case -2:
				throw new RegulationCorpUserFromBranchException(resultInteger,
						"字段corpId不能为空!");
			case -3:
				throw new RegulationCorpUserFromBranchException(resultInteger,
						"部门不存在!");
			case -4:
				throw new RegulationCorpUserFromBranchException(resultInteger,
						"待调整人员必须为企业人员!");
			default:
				throw new RegulationCorpUserFromBranchException(resultInteger,
						OTHER_ERROR);
			}
		} catch (ConnectException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new RegulationCorpUserFromBranchException(e.getMessage(), e);
		}
	}

	public String getCorpbranchsByPid(Branchs branchs) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(branchs);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}

	public String getBranchUsersByBid(BranchUsers branchUsers) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(branchUsers);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}

	public int branchSort(String jaArray) {
		String res = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder.encode(jaArray.toString(),
					"UTF-8"));
			res = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(res);
			}
			Integer resultInteger = Integer.valueOf(res);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -1:
				throw new AddrBookException(resultInteger, "部门不存在");
			default:
				throw new AddrBookException(resultInteger, OTHER_ERROR);
			}
		} catch (JSONException e) {
			throw new CorpException("没有返回预期的数据!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SyncEventException(e.getMessage(), e);
		}
	}
	
	public int corpUserSort(String jaArray) {
		String res = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder.encode(jaArray.toString(),
					"UTF-8"));
			res = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(res);
			}
			Integer resultInteger = Integer.valueOf(res);
			if (resultInteger > 0) {
				return resultInteger;
			}
			switch (resultInteger) {
			case -1:
				throw new AddrBookException(resultInteger, "用户不存在");
			default:
				throw new AddrBookException(resultInteger, OTHER_ERROR);
			}
		} catch (JSONException e) {
			throw new CorpException("没有返回预期的数据!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SyncEventException(e.getMessage(), e);
		}
	}
}
