package cn.com.focu.im.sdk.service.user.entity;

public class UserLoginNameExists {
	private String loginName;
	private Integer uid;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public UserLoginNameExists(String loginName, Integer uid) {
		super();
		this.loginName = loginName;
		this.uid = uid;
	}
	public UserLoginNameExists(String loginName) {
		super();
		this.loginName = loginName;
	}
}
