package cn.com.focu.im.sdk.service.msg;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.msg.entity.SysMessage;
import cn.com.focu.im.sdk.service.msg.exception.SysMessageException;

@Operator("msg")
public class SysMessageOperator extends ServiceOperator {

	public SysMessageOperator() {
	}

	private static class SysMessageOpeatorHolder {
		static SysMessageOperator smo = new SysMessageOperator();
	}

	public static SysMessageOperator getInstance() {
		return SysMessageOpeatorHolder.smo;
	}

	public Integer sendMessageToUsers(SysMessage sm) throws SysMessageException {
		try {
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("sender", sm.getSender());
			paramsMap.put("display", sm.getDisplay() + "");
			paramsMap.put("closeDelay", sm.getCloseDelay() + "");
			paramsMap.put("showDelay", sm.getShowDelay() + "");
			paramsMap.put("announceType", sm.getAnnounceType() + "");
			paramsMap.put("frequency", sm.getFrequency() + "");
			paramsMap.put("title", sm.getTitle());
			paramsMap.put("content", sm.getContent());
			paramsMap.put("receivers", sm.getReceivers());
			paramsMap.put("position", sm.getPosition() + "");
			paramsMap.put("height", sm.getHeight() + "");
			paramsMap.put("width", sm.getWidth() + "");

			String re = postReturnString(paramsMap);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer status = Integer.valueOf(re);
			if (status >= 0) {
				return status;
			}
			switch (status) {
			case -1:
				throw new SysMessageException(status, "receivers,不能为空！");
			default:
				throw new SysMessageException(status, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SysMessageException(e.getMessage(), e);
		}
	}

	/**
	 * 保存企业公告/广播消息
	 * 
	 * @param code
	 *            企业代码
	 * @param sm
	 *            发送消息对象
	 * @return 0成功，-1失败
	 * @throws SysMessageException
	 */
	public Integer saveCorpAnnounce(String code, SysMessage sm)
			throws SysMessageException {
		try {
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("code", code);
			paramsMap.put("sender", sm.getSender());
			paramsMap.put("display", sm.getDisplay() + "");
			paramsMap.put("closeDelay", sm.getCloseDelay() + "");
			paramsMap.put("showDelay", sm.getShowDelay() + "");
			paramsMap.put("announceType", sm.getAnnounceType() + "");
			paramsMap.put("frequency", sm.getFrequency() + "");
			paramsMap.put("title", sm.getTitle());
			paramsMap.put("content", sm.getContent());
			paramsMap.put("receivers", sm.getReceivers());
			paramsMap.put("position", sm.getPosition() + "");
			paramsMap.put("height", sm.getHeight() + "");
			paramsMap.put("width", sm.getWidth() + "");
			paramsMap.put("expiredDate", sm.getExpiredDate() + "");
			paramsMap.put("effectiveDate", sm.getEffectiveDate() + "");
			paramsMap.put("receiveType", sm.getReceiveType() + "");
			paramsMap.put("realTime", sm.isRealTime() + "");
			paramsMap.put("url", sm.getUrl());
			paramsMap.put("annReceiveType", sm.getAnnReceiveType() + "");
			
			String re = postReturnString(paramsMap);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer status = Integer.valueOf(re);
			if (status >= 0) {
				return status;
			}
			switch (status) {
			case -1:
				throw new SysMessageException(status, "receivers,不能为空！");
			default:
				throw new SysMessageException(status, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SysMessageException(e.getMessage(), e);
		}
	}

	/**
	 * 根据企业代码获取该企业下的消息列表
	 * 
	 * @param code
	 *            企业代码
	 * @param pageNo
	 *            页码
	 * @param pageSize
	 *            每页显示记录数
	 * @param type
	 *            消息类型[1:发送记录,0:保存记录]
	 * @return 分页后的消息列表
	 * 
	 * @throws SysMessageException
	 */
	public String getMessages(String code, Integer pageNo, Integer pageSize,
			Integer type) throws SysMessageException {
		String re = "";
		try {
			Map<String, Object> paramsMap = new HashMap<String, Object>();
			paramsMap.put("code", code);
			paramsMap.put("pageNo", pageNo + "");
			paramsMap.put("pageSize", pageSize + "");
			paramsMap.put("type", type + "");

			re = postReturnString(paramsMap);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SysMessageException(e.getMessage(), e);
		}
		return re;
	}

	/**
	 * 根据公告/广播消息id得到详细信息
	 * 
	 * @param announceId
	 *            公告/广播消息id
	 * @return
	 * @throws SysMessageException
	 */
	public String getCorpAnnounceInfoById(Integer announceId)
			throws SysMessageException {
		String re = "";
		try {
			Map<String, Object> paramsMap = new HashMap<String, Object>();

			paramsMap.put("announceId", announceId + "");

			re = postReturnString(paramsMap);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new SysMessageException(e.getMessage(), e);
		}
		return re;
	}
}
