package cn.com.focu.im.sdk.service.group.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class GroupException extends ServiceException {

	private static final long serialVersionUID = -238553788834122780L;

	public GroupException(int code, String message) {
		super(code, message);
	}

	public GroupException(String message, Throwable cause) {
		super(message, cause);
	}

}
