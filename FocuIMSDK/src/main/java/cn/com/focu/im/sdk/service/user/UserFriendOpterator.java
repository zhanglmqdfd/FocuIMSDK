package cn.com.focu.im.sdk.service.user;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.user.entity.UserFriendAdd;
import cn.com.focu.im.sdk.service.user.entity.UserFriendDelete;
import cn.com.focu.im.sdk.service.user.entity.UserFriendGet;
import cn.com.focu.im.sdk.service.user.entity.UserMessage;
import cn.com.focu.im.sdk.service.user.exception.UserFriendAddException;
import cn.com.focu.im.sdk.service.user.exception.UserFriendDeleteException;
import cn.com.focu.im.sdk.service.user.exception.UserFriendGetException;
import cn.com.focu.im.sdk.service.user.exception.UserFriendMessageException;

/**
 * @author zhangjie
 */
@Operator("ufriend")
public class UserFriendOpterator extends ServiceOperator {
	public UserFriendOpterator() {
	}

	private static class UserFriendOperatorHoder {
		static UserFriendOpterator userFriendOpterator = new UserFriendOpterator();
	}

	public static UserFriendOpterator getInstance() {
		return UserFriendOperatorHoder.userFriendOpterator;
	}

	/**
	 * 添加好友
	 * 
	 * @param ufa
	 * @return
	 * @throws UserFriendAddException
	 */
	public Integer add(UserFriendAdd ufa) throws UserFriendAddException {
		try {
			JSONObject json = JSONObject.fromObject(ufa);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status >= -1)
				return status;
			switch (status) {
			/*
			 * case 0 : throw new UserFriendAddException("添加的用户不存在"); case -1:
			 * throw new UserFriendAddException("该用户已经是的好友");
			 */
			case -2:
				throw new UserFriendAddException(status, "要添加的数据为没有顺利提交到后台");
			default:
				throw new UserFriendAddException(status, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			new UserFriendAddException(e.getMessage(), e);
		}
		return -2;
	}

	/**
	 * 删除好友
	 * 
	 * @param ufd
	 * @return
	 * @throws UserFriendDeleteException
	 */
	public Integer delete(UserFriendDelete ufd)
			throws UserFriendDeleteException {
		try {
			JSONObject json = JSONObject.fromObject(ufd);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status >= -1) {
				return status;
			}
			switch (status) {
			/*
			 * case -1: throw new UserFriendDeleteException("您要删除的用户还不是您的好友");
			 * case 0: throw new
			 * UserFriendDeleteException("用户userid不能为0或该用户不存在");
			 */
			case -2:
				throw new UserFriendDeleteException(status, "数据传输不稳定，请稍候再进行操作");
			default:
				throw new UserFriendDeleteException(status, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new UserFriendDeleteException(e.getMessage(), e);
		}
	}

	/**
	 * 获取朋友列表
	 * 
	 * @param ufg
	 * @return
	 */
	public String get(UserFriendGet ufg) throws UserFriendGetException {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(ufg);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			switch (status) {
			case 0:
				throw new UserFriendGetException(status, "获取数据失败");
			default:
				throw new UserFriendGetException(status, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			new UserFriendGetException(e.getMessage(), e);
		}
		return rs;
	}

	public Integer friendMessage(UserMessage uMessage)
			throws UserFriendDeleteException {
		try {
			JSONObject json = JSONObject.fromObject(uMessage);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status >= -1) {
				return status;
			}
			switch (status) {
			case 0:
				throw new UserFriendMessageException(status, "数据传输不稳定，请稍候再进行操作");
			case -1:
				throw new UserFriendMessageException(status, "发送者用户账号不存在");
			case -2:
				throw new UserFriendMessageException(status, "接收者用户账号不存在");
			default:
				throw new UserFriendMessageException(status, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new UserFriendMessageException(e.getMessage(), e);
		}
	}
}
