package cn.com.focu.im.sdk.service.group;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.group.entity.SelfGroup;

@Operator("group")
public class SelfGroupOperator extends ServiceOperator {

	public SelfGroupOperator(){
		
	}
	
	public static class SelfGroupOperatorHolder {
		static SelfGroupOperator go = new SelfGroupOperator();
	}

	public static SelfGroupOperator getInstence() {
		return SelfGroupOperatorHolder.go;
	}
	
	
	public String getSelfGroupByPid(SelfGroup sg){
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(sg);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}
}
