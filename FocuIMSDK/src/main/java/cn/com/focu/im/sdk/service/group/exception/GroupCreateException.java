package cn.com.focu.im.sdk.service.group.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class GroupCreateException extends ServiceException {

	private static final long serialVersionUID = 103636358514751206L;

	public GroupCreateException(int code, String message) {
		super(code, message);
	}

	public GroupCreateException(String message, Throwable cause) {
		super(message, cause);
	}
}
