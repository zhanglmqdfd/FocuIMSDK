package cn.com.focu.im.sdk.service.pendingApp.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class PendingAppException extends ServiceException {

	private static final long serialVersionUID = 8139890252739800201L;

	public PendingAppException(int code, String message) {
		super(code, message);
	}

	public PendingAppException(String message, Throwable cause) {
		super(message, cause);
	}
}
