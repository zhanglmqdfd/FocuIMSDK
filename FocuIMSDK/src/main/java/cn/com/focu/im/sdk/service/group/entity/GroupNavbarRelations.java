package cn.com.focu.im.sdk.service.group.entity;

public class GroupNavbarRelations {

	private Integer navbarId;
	private Integer[] groupId;
	private Integer online;
	private Integer currentSize;
	
	private Integer pageNo;
	private Integer pageSize;
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getCurrentSize() {
		return currentSize;
	}
	public void setCurrentSize(Integer currentSize) {
		this.currentSize = currentSize;
	}
	public Integer getOnline() {
		return online;
	}
	public void setOnline(Integer online) {
		this.online = online;
	}
	public Integer getNavbarId() {
		return navbarId;
	}
	public void setNavbarId(Integer navbarId) {
		this.navbarId = navbarId;
	}
	public Integer[] getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer[] groupId) {
		this.groupId = groupId;
	}
}
