package cn.com.focu.im.sdk.service.msg.entity;


public class SysMessage {
	private String sender;
	private Integer closeDelay = 0;
	private Integer showDelay =0 ;
	private Integer announceType = 1;
	private Integer frequency =1 ;
	private String receivers;
	private Integer position =1;
	private Integer height;
	private Integer width;
	private Integer display = 0;
	
	//--------新增
	private String expiredDate;
	private String effectiveDate;
	private Integer receiveType;
	private boolean realTime;
	private String url;
	private Integer annReceiveType;
	
	public Integer getAnnReceiveType() {
		return annReceiveType;
	}
	public void setAnnReceiveType(Integer annReceiveType) {
		this.annReceiveType = annReceiveType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isRealTime() {
		return realTime;
	}
	public void setRealTime(boolean realTime) {
		this.realTime = realTime;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Integer getReceiveType() {
		return receiveType;
	}
	public void setReceiveType(Integer receiveType) {
		this.receiveType = receiveType;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public Integer getCloseDelay() {
		return closeDelay;
	}
	public void setCloseDelay(Integer closeDelay) {
		this.closeDelay = closeDelay;
	}
	public Integer getShowDelay() {
		return showDelay;
	}
	public void setShowDelay(Integer showDelay) {
		this.showDelay = showDelay;
	}
	public Integer getAnnounceType() {
		return announceType;
	}
	public void setAnnounceType(Integer announceType) {
		this.announceType = announceType;
	}
	public Integer getFrequency() {
		return frequency;
	}
	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
	public String getReceivers() {
		return receivers;
	}
	public void setReceivers(String receivers) {
		this.receivers = receivers;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getDisplay() {
		return display;
	}
	public void setDisplay(Integer display) {
		this.display = display;
	}

	private String content;
	private String title;
}
