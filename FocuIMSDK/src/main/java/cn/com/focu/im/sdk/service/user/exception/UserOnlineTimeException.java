package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

/**
 * 获取在线时长异常
 * 
 * @author yeabow
 * 
 */
public class UserOnlineTimeException extends ServiceException {

	private static final long serialVersionUID = 385785644397733318L;

	public UserOnlineTimeException(int code, String message) {
		super(code, message);
	}

	public UserOnlineTimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
