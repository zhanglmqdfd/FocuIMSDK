package cn.com.focu.im.sdk.service.industryApps.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class IndustryAppsException extends ServiceException {

	private static final long serialVersionUID = 8139890252739800201L;

	public IndustryAppsException(int code, String message) {
		super(code, message);
	}

	public IndustryAppsException(String message, Throwable cause) {
		super(message, cause);
	}
}
