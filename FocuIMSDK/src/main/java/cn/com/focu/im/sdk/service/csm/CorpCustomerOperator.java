package cn.com.focu.im.sdk.service.csm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.csm.entity.CorpCustomer;
import cn.com.focu.im.sdk.service.csm.entity.CustomersReturnPage;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;

@Operator("corpCustomer")
public class CorpCustomerOperator extends ServiceOperator {

	private CorpCustomerOperator() {

	}

	private static class CorpCustomerOperatorHolder {
		static CorpCustomerOperator corpCustomerOperator = new CorpCustomerOperator();
	}

	public static CorpCustomerOperator getInstance() {
		return CorpCustomerOperatorHolder.corpCustomerOperator;
	}

	/**
	 * 增加企业客服
	 * 
	 * @param corpCustomer
	 * @return
	 * @throws Exception
	 */
	public int saveCorpCustomer(CorpCustomer corpCustomer) throws Exception {
		try {
			JSONObject json = JSONObject.fromObject(corpCustomer);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);

			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}

			switch (status) {
			case 0:
				throw new CorpCustomerException(status,
						"数据提交出现异常：\n\t1.用户没有指定要添加到哪个企业,\n\t2.没有指定待添加的用户");
			default:
				throw new CorpCustomerException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new CorpCustomerException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new CorpCustomerException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 删除企业客服
	 * 
	 * @param corpCustomer
	 * @return
	 * @throws Exception
	 */
	public int delCorpCustomer(CorpCustomer corpCustomer) throws Exception {
		try {
			JSONObject json = JSONObject.fromObject(corpCustomer);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);

			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}

			switch (status) {
			case 0:
				throw new CorpCustomerException(status,
						"数据提交出现异常：\n\t1.用户没有指定要删除企业下的用户,\n\t2.没有指定待删除的用户");
			default:
				throw new CorpCustomerException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new CorpCustomerException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new CorpCustomerException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 分页得到企业客服
	 * 
	 * @param corpId
	 * @return
	 * @throws Exception
	 */
	public String getCustomersReturnPage(CustomersReturnPage customersReturnPage)
			throws Exception {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(customersReturnPage);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}
}
