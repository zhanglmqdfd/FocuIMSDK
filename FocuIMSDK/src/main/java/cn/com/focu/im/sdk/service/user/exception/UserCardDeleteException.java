package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;


public class UserCardDeleteException extends ServiceException {

	private static final long serialVersionUID = -963592067079355133L;

	public UserCardDeleteException(int code, String message) {
		super(code, message);
	}

	public UserCardDeleteException(String message, Throwable cause) {
		super(message, cause);
	}
}
