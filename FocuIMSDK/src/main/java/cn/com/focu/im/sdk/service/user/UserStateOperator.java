package cn.com.focu.im.sdk.service.user;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.user.entity.UserStateGet;

@Operator("ustate")
public class UserStateOperator extends ServiceOperator {

	public UserStateOperator() {
	}

	// 创建单利模式
	public static class UserStateOperatorHolder {
		static UserStateOperator userStateOperator = new UserStateOperator();
	}

	public static UserStateOperator getinstance() {
		return UserStateOperatorHolder.userStateOperator;
	}

	/**
	 * 获得用户状态类型
	 * 
	 * @return
	 */
	public String getStateTypes() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			return rs;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	// 根据用户名或者userid获取该用户状态
	public String get(UserStateGet usg) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(usg);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
		}
		return rs;
	}

}
