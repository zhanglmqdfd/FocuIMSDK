package cn.com.focu.im.sdk.service.group.entity;

public class GroupCreate {

	private String gname;

	private Integer glevel;

	private Integer gtype;

	private Integer flag = 0;

	private Integer uid; // 群创始人

	private String loginNumber;

	private String loginName;

	private String notice; // 设置群公告
	private String intro; // 设置群简介

	private Integer validate; // 设置群验证方式

	private Object[] users; // 群成员,存入用户id

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public Integer getGlevel() {
		return glevel;
	}

	public void setGlevel(Integer glevel) {
		this.glevel = glevel;
	}

	public Integer getGtype() {
		return gtype;
	}

	public void setGtype(Integer gtype) {
		this.gtype = gtype;
	}

	public Integer getFlag() {
		return flag;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
		this.flag = 1;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
		this.flag = 2;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
		this.flag = 0;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Integer getValidate() {
		return validate;
	}

	public void setValidate(Integer validate) {
		this.validate = validate;
	}

	public Object[] getUsers() {
		return users;
	}

	public void setUsers(Object[] users) {
		this.users = users;
	}

	public void setUsers(String[] users) {
		this.users = users;
	}

	public void setUsers(Integer[] users) {
		this.users = users;
	}
}
