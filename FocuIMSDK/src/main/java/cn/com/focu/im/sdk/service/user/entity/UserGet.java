package cn.com.focu.im.sdk.service.user.entity;

public class UserGet {
	private String loginName;

	private String loginNumber;
	
	private String email;
	
	private String mobile;

	private Integer uid;

	private int flag = 0;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
		this.flag = 0;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.loginNumber = loginNumber;
		this.flag = 2;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
		this.flag = 1;
	}

	public int getFlag() {
		return flag;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.flag = 3;
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.flag = 4;
		this.mobile = mobile;
	}
}
