package cn.com.focu.im.sdk.service.group.entity;

public class GroupNavbarSort {
	
	private Integer mNavbarId;
	
	private Integer yNavbarId;
	
	private Integer orientation;//移动方向0：置顶 、1：向上或向下 、2：置末
	
	private Integer flg;

	public Integer getFlg() {
		return flg;
	}

	public void setFlg(Integer flg) {
		this.flg = flg;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}

	public Integer getmNavbarId() {
		return mNavbarId;
	}

	public void setmNavbarId(Integer mNavbarId) {
		this.mNavbarId = mNavbarId;
	}

	public Integer getyNavbarId() {
		return yNavbarId;
	}

	public void setyNavbarId(Integer yNavbarId) {
		this.yNavbarId = yNavbarId;
	}
	
	
}
