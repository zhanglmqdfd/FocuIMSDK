package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserLoginNumberExistsException extends ServiceException {

	private static final long serialVersionUID = -3070400299118489268L;

	public UserLoginNumberExistsException(int code, String message) {
		super(code, message);
	}

	public UserLoginNumberExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
