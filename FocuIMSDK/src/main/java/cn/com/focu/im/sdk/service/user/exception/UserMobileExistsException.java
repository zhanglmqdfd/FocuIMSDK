package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserMobileExistsException extends ServiceException {

	private static final long serialVersionUID = -6663976551164881230L;

	public UserMobileExistsException(int code, String message) {
		super(code, message);
	}

	public UserMobileExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
