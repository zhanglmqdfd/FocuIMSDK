package cn.com.focu.im.sdk.service.group.entity;

public class GroupUpdate {
	private Integer gid;
	private String gname;
	private Integer glevel;
	private Integer gtype;
	
	private Integer uid; //群创始人
	
	private String notice; //设置群公告
	private String intro;  //设置群简介
	
	private Integer validate; // 设置群验证方式

	/**
	 * @return the gid
	 */
	public Integer getGid() {
		return gid;
	}

	/**
	 * @param gid the gid to set
	 */
	public void setGid(Integer gid) {
		this.gid = gid;
	}

	/**
	 * @return the gname
	 */
	public String getGname() {
		return gname;
	}

	/**
	 * @param gname the gname to set
	 */
	public void setGname(String gname) {
		this.gname = gname;
	}

	/**
	 * @return the glevel
	 */
	public Integer getGlevel() {
		return glevel;
	}

	/**
	 * @param glevel the glevel to set
	 */
	public void setGlevel(Integer glevel) {
		this.glevel = glevel;
	}

	/**
	 * @return the gtype
	 */
	public Integer getGtype() {
		return gtype;
	}

	/**
	 * @param gtype the gtype to set
	 */
	public void setGtype(Integer gtype) {
		this.gtype = gtype;
	}

	/**
	 * @return the uid
	 */
	public Integer getUid() {
		return uid;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}

	/**
	 * @return the intro
	 */
	public String getIntro() {
		return intro;
	}

	/**
	 * @param intro the intro to set
	 */
	public void setIntro(String intro) {
		this.intro = intro;
	}

	/**
	 * @return the validate
	 */
	public Integer getValidate() {
		return validate;
	}

	/**
	 * @param validate the validate to set
	 */
	public void setValidate(Integer validate) {
		this.validate = validate;
	}
	

}
