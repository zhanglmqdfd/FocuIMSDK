package cn.com.focu.im.sdk.service.pendingApp;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbar;
import cn.com.focu.im.sdk.service.group.exception.GroupNavbarException;
import cn.com.focu.im.sdk.service.industryApps.IndustryAppsOperator;
import cn.com.focu.im.sdk.service.industryApps.entity.AddIndustryApps;
import cn.com.focu.im.sdk.service.industryApps.exception.IndustryAppsException;
import cn.com.focu.im.sdk.service.pendingApp.entity.AddPending;
import cn.com.focu.im.sdk.service.pendingApp.entity.AddPendingState;
import cn.com.focu.im.sdk.service.pendingApp.entity.DeletePending;
import cn.com.focu.im.sdk.service.pendingApp.entity.GetPendingApp;
import cn.com.focu.im.sdk.service.pendingApp.exception.PendingAppException;

@Operator("pending")
public class PendingAppOperator extends ServiceOperator {
	public PendingAppOperator() {
	}

	public static class PendingAppOperatorHolder {
		static PendingAppOperator go = new PendingAppOperator();
	}

	public static PendingAppOperator getInstence() {
		return PendingAppOperatorHolder.go;
	}
	
	
	public String getPendingAppReturnPage(GetPendingApp getPending) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(getPending);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}

	public String add(AddPending AddPending) {
		try {
			JSONObject json = JSONObject.fromObject(AddPending);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			return re;
		} catch (NumberFormatException e) {
			throw new PendingAppException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new PendingAppException(e.getMessage(), e);
		}
	}

	public String delete(DeletePending deletePending) {
		try {
			JSONObject json = JSONObject.fromObject(deletePending);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			return re;
		} catch (NumberFormatException e) {
			throw new PendingAppException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new PendingAppException(e.getMessage(), e);
		}
	}

	public String addState(AddPendingState AddPendingState) {
		try {
			JSONObject json = JSONObject.fromObject(AddPendingState);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			return re;
		} catch (NumberFormatException e) {
			throw new PendingAppException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new PendingAppException(e.getMessage(), e);
		}
	}
}
