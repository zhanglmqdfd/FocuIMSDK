package cn.com.focu.im.sdk.service;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.com.focu.commons.util.ConfigUtil;
import cn.com.focu.commons.util.HttpClientCallback;
import cn.com.focu.commons.util.HttpClientUtil;
import cn.com.focu.im.sdk.service.annotation.Operator;

/**
 * 服务操作基类
 * 
 * @作者 yeabow
 * @创建日期 Jul 12, 2011 5:11:34 PM
 * @版本 v1.0.0
 */
public abstract class ServiceOperator {

	protected final String CANNOT_CONNECT_TO_SERVER = "无法连接到服务器,请检查"
			+ PROP_FILE_NAME + "中的" + PROP_INTERFACE_URL + "配置信息,确认为正确的服务器地址!";

	protected final String OTHER_ERROR = "其他错误!可能是由于您的接口客户端版本太旧造成的!请联系服务提供者!";

	private static String INTERFACE_URL = "http://locahost/FocuIMManage/";

	private static String INTERFACE_OPERATOR_PATH = "httpservices/";

	private static String INTERFACE_EXTENSION = ".action";

	private static final String PROP_INTERFACE_URL = "im.server";

	private static final String PROP_INTERFACE_OPERATOR_PATH = "im.operator.path";

	private static final String PROP_FILE_NAME = "im.properties";

	private static final String PROP_INTERFACE_EXTENSION = "im.extension";

	private static Properties config = null;

	protected Log log = LogFactory.getLog(getClass());

	static {
		config = ConfigUtil.getPropertiesForClassPathOrJarFile(PROP_FILE_NAME);
		if (config != null) {
			INTERFACE_URL = config.getProperty(PROP_INTERFACE_URL,
					INTERFACE_URL);
			INTERFACE_OPERATOR_PATH = config.getProperty(
					PROP_INTERFACE_OPERATOR_PATH, INTERFACE_OPERATOR_PATH);
			INTERFACE_EXTENSION = config.getProperty(PROP_INTERFACE_EXTENSION,
					INTERFACE_EXTENSION);
		}
	}

	public static String getIMServer() {
		return INTERFACE_URL;
	}

	/**
	 * 指定接口访问地址
	 * 
	 * @param interface_url
	 */
	public static void setInterfaceUrl(String interface_url) {
		INTERFACE_URL = interface_url;
	}
	
	protected static Map<String, String> cachedMethodName = new ConcurrentHashMap<String, String>();
	
	public static String getInterfaceName() {
		StackTraceElement[] stacks = new Throwable().getStackTrace();
		try {
			StackTraceElement stack = stacks[2];
			String orgMethodName = stack.getMethodName();
			String orgClassName = stack.getClassName();
			String key = orgClassName + "-" + orgMethodName;
			if (!cachedMethodName.containsKey(key)) {
				Class<?> clazz = Class.forName(orgClassName);
				if (clazz.isAnnotationPresent(Operator.class)) {
					Operator operator = clazz.getAnnotation(Operator.class);
					String methodName = config
							.getProperty("im.operator." + operator.value() + "."
									+ orgMethodName, operator.value()
									+ "-" + orgMethodName
									+ INTERFACE_EXTENSION);
					cachedMethodName.put(key, methodName);
					return methodName;
				}
			} else {
				return cachedMethodName.get(key);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Post方式请求，返回String.
	 * 
	 * @param params
	 *            参数集合
	 * @return 返回值
	 * @throws Exception
	 */
	public String postReturnString(Map<String, Object> params)
			throws ConnectException, IOException {
		String interfaceUrl = INTERFACE_URL + INTERFACE_OPERATOR_PATH
				+ getInterfaceName();
		return HttpClientUtil.postReturnString(interfaceUrl, params);
	}

	/**
	 * Post方式请求，返回Stream.
	 * 
	 * @param params
	 *            参数集合
	 * @return 返回值
	 * @throws Exception
	 */
	public Object postReturnStream(Map<String, Object> params,
			HttpClientCallback callback) throws ConnectException, IOException {
		String interfaceUrl = INTERFACE_URL + INTERFACE_OPERATOR_PATH
				+ getInterfaceName();
		return HttpClientUtil.postReturnStream(interfaceUrl, params, callback);
	}

	public String postFilesToServer(Part[] parts) throws ConnectException,
			IOException {
		String interfaceUrl = INTERFACE_URL + INTERFACE_OPERATOR_PATH
				+ getInterfaceName();
		return HttpClientUtil.postFilesToServer(interfaceUrl, parts);
	}

	public static void main(String[] args) {
		System.out.println(INTERFACE_URL);
	}
}
