package cn.com.focu.im.sdk.service.sso.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class SSOAuthorityException extends ServiceException {

	private static final long serialVersionUID = -1252257174223405847L;

	public SSOAuthorityException(int code, String message) {
		super(code, message);
	}

	public SSOAuthorityException(String message, Throwable cause) {
		super(message, cause);
	}
}
