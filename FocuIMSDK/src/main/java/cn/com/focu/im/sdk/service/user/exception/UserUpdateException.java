package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserUpdateException extends ServiceException{

	private static final long serialVersionUID = -3762390106076699925L;

	public UserUpdateException(int code, String message) {
		super(code, message);
	}

	public UserUpdateException(String message, Throwable cause) {
		super(message, cause);
	}
}
