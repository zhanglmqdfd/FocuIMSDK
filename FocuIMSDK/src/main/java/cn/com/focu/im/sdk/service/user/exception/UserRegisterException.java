package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

/**
 * 用户注册异常
 * 
 * @作者 yeabow
 * @创建日期 Jul 12, 2011 5:13:11 PM
 * @版本 v1.0.0
 */
public class UserRegisterException extends ServiceException {

	private static final long serialVersionUID = 7923057025063893144L;

	public UserRegisterException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}

	public UserRegisterException(int code, String message) {
		super(code, message);
	}
}
