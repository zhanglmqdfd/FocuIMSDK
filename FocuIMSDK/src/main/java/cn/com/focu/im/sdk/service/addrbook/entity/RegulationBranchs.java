package cn.com.focu.im.sdk.service.addrbook.entity;

public class RegulationBranchs {
	private Integer corpId;
	private String corpCode;
	private String bcode;
	private String pcode;
	
	public RegulationBranchs(Integer corpId, String bcode, String pcode) {
		super();
		this.corpId = corpId;
		this.bcode = bcode;
		this.pcode = pcode;
	}
	
	public RegulationBranchs(String corpCode, String bcode, String pcode) {
		super();
		this.corpCode = corpCode;
		this.bcode = bcode;
		this.pcode = pcode;
	}

	public Integer getCorpId() {
		return corpId;
	}
	
	public String getCorpCode() {
		return corpCode;
	}

	public String getBcode() {
		return bcode;
	}

	public String getPcode() {
		return pcode;
	}
}
