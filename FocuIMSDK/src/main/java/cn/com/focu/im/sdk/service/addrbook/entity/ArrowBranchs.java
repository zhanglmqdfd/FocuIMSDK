package cn.com.focu.im.sdk.service.addrbook.entity;

import cn.com.focu.im.sdk.service.addrbook.exception.ArrowException;

public class ArrowBranchs extends Arrow {

	private Integer flag = 0;

	private Integer corpId;

	private String corpCode;

	private String bcode;

	private String pcode;

	public ArrowBranchs(String direction, String corpCode, String bcode) {
		super(direction);
		if (!direction.equals("T") && !direction.equals("B"))
			throw new ArrowException(-1, "该构造只允许移动到[T|B]");
		this.corpCode = corpCode;
		this.bcode = bcode;
		this.flag = 1; 
	}

	public ArrowBranchs(String direction, String corpCode, String bcode,
			String pcode) {
		super(direction);
		if (!direction.equals("U") && !direction.equals("D"))
			throw new ArrowException(-1, "该构造只允许移动到[U|D]");
		this.corpCode = corpCode;
		this.bcode = bcode;
		this.pcode = pcode;
		this.flag = 1;
	}

	public ArrowBranchs(String direction, Integer corpId, String bcode,
			String pcode) {
		super(direction);
		if (!direction.equals("U") && !direction.equals("D"))
			throw new ArrowException(-1, "该构造只允许移动到[U|D]");
		this.corpId = corpId;
		this.bcode = bcode;
		this.pcode = pcode;
		this.flag = 0;
	}

	public ArrowBranchs(String direction, Integer corpId, String bcode) {
		super(direction);
		if (!direction.equals("T") && !direction.equals("B"))
			throw new ArrowException(-1, "该构造只允许移动到[T|B]");
		this.corpId = corpId;
		this.bcode = bcode;
		this.flag = 0;
	}

	public Integer getFlag() {
		return flag;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public String getBcode() {
		return bcode;
	}

	public String getPcode() {
		return pcode;
	}
}
