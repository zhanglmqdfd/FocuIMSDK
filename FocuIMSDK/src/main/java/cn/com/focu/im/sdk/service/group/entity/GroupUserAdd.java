package cn.com.focu.im.sdk.service.group.entity;

public class GroupUserAdd {
	
	private Integer flag;
	/**
	 * @return the flag
	 */
	public Integer getFlag() {
		return flag;
	}
	/**
	 * @param flag the flag to set
	 */
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	/**
	 * @return the groups
	 */
	public Integer[] getGroups() {
		return groups;
	}
	/**
	 * @param groups the groups to set
	 */
	public void setGroups(Integer[] groups) {
		this.groups = groups;
	}
	/**
	 * @return the users
	 */
	public Integer[] getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(Integer[] users) {
		this.users = users;
	}
	private Integer [] groups;
	private Integer [] users;
	

}
