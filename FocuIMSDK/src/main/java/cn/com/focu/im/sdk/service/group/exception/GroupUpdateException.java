package cn.com.focu.im.sdk.service.group.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class GroupUpdateException extends ServiceException {
	private static final long serialVersionUID = -5243129079918012633L;

	public GroupUpdateException(int code, String message) {
		super(code, message);
	}

	public GroupUpdateException(String message, Throwable cause) {
		super(message, cause);
	}
}
