package cn.com.focu.im.sdk.service.addrbook.entity;

import org.apache.commons.lang.StringUtils;

import cn.com.focu.im.sdk.service.addrbook.exception.ArrowException;

public class Arrow {
	private String direction;

	public Arrow(String direction) {
		super();
		if (StringUtils.isBlank(direction)
				|| (!direction.equals("U") && !direction.equals("D")
						&& !direction.equals("T") && !direction.equals("B")))
			throw new ArrowException(-1,
					"找不到移动方向字段[direction]或是方向错误!不属于[U|D|T|B]");
		
		this.direction = direction;
	}

	public String getDirection() {
		return direction;
	}
}
