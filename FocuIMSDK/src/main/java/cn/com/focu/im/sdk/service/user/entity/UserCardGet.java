package cn.com.focu.im.sdk.service.user.entity;

public class UserCardGet {
	
	
	private Integer uid;
	private Integer tid;
    private Integer gid;
    
    
    
    
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public Integer getTid() {
		return tid;
	}
	public void setTid(Integer tid) {
		this.tid = tid;
	}
	public Integer getGid() {
		return gid;
	}
	public void setGid(Integer gid) {
		this.gid = gid;
	}
}
