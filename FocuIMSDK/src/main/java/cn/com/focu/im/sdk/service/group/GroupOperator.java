package cn.com.focu.im.sdk.service.group;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.group.entity.GroupCreate;
import cn.com.focu.im.sdk.service.group.entity.GroupSort;
import cn.com.focu.im.sdk.service.group.entity.GroupUpdate;
import cn.com.focu.im.sdk.service.group.entity.GroupUserAdd;
import cn.com.focu.im.sdk.service.group.entity.GroupUserDelete;
import cn.com.focu.im.sdk.service.group.entity.Groups;
import cn.com.focu.im.sdk.service.group.exception.GroupCreateException;
import cn.com.focu.im.sdk.service.group.exception.GroupException;
import cn.com.focu.im.sdk.service.group.exception.GroupNavbarException;
import cn.com.focu.im.sdk.service.group.exception.GroupUpdateException;
import cn.com.focu.im.sdk.service.group.exception.GroupUserAddException;
import cn.com.focu.im.sdk.service.user.entity.GetGroupByUser;

/**
 * @author 张杰 time：2011-09-19
 */
@Operator("group")
public class GroupOperator extends ServiceOperator {

	public GroupOperator() {
	}

	public static class GroupOperatorHolder {
		static GroupOperator go = new GroupOperator();
	}

	public static GroupOperator getInstence() {
		return GroupOperatorHolder.go;
	}

	/**
	 * 根据关键字获取群
	 * 
	 * @param pageSize
	 * @param current
	 * @param keyWord
	 * @return
	 */
	public String getByKeyWord(int pageSize, int current, String keyWord) {
		JSONObject json = new JSONObject();
		json.put("pageSize", pageSize);
		json.put("current", current);
		json.put("keyWord", keyWord);
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	/**
	 * 根据userId获取当前群列表
	 * 
	 * @param userId
	 * @return
	 */
	public String getByUserId(GetGroupByUser getGroupByUser) {
		try {
			JSONObject json = JSONObject.fromObject(getGroupByUser);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			return rs;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
	}

	/**
	 * 获取某群相关信息
	 * 
	 * @param groupId
	 * @return
	 */
	public String getByGroupId(Integer groupId) {
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("groupId", URLEncoder
					.encode(groupId.toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	public String getByGroupNumber(Integer groupNumber) {
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("groupNumber", URLEncoder.encode(groupNumber.toString(),
					"UTF-8"));
			params.put("flag", "1");
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	/**
	 * 根据城市获取群
	 * 
	 * @return
	 */
	public String getByCity(int pageSize, int current, int cityId) {
		JSONObject json = new JSONObject();
		if (pageSize == 0)
			pageSize = 1;
		json.put("pageSize", pageSize);
		json.put("current", current);
		json.put("cityId", cityId);
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	/**
	 * 根据省获取群
	 * 
	 * @return
	 */
	public String getByProvince(int pageSize, int current, int provinceId) {
		JSONObject json = new JSONObject();
		if (pageSize == 0)
			pageSize = 1;
		json.put("pageSize", pageSize);
		json.put("current", current);
		json.put("provinceId", provinceId);
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	/**
	 * 根据市场获取群
	 * 
	 * @return
	 */
	public String getByMarket(int pageSize, int current, String market) {
		JSONObject json = new JSONObject();
		if (pageSize == 0)
			pageSize = 1;
		json.put("pageSize", pageSize);
		json.put("current", current);
		json.put("market", market);
		String str = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	/**
	 * 根据群类型获取群
	 * 
	 * @param pageSize
	 *            分页大小
	 * @param current
	 *            当前页
	 * @param groupTypeId
	 * @return
	 * @throws GroupException
	 */
	public String getByType(int pageSize, int current, int groupTypeId)
			throws GroupException {
		String str = "";
		JSONObject json = new JSONObject();
		if (pageSize == 0)
			pageSize = 1;
		json.put("groupTypeId", groupTypeId);
		json.put("pageSize", pageSize);
		json.put("current", current);
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			str = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(str);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return str;
	}

	/**
	 * 获取群分类列表
	 * 
	 * @return
	 * @throws GroupException
	 */
	public String getCategories() throws GroupException {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 根据群的父id,获取该父id下的子类型
	 * 
	 * @param parentid
	 * @return
	 */
	public String getTypes(Integer parentid) {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("parent", URLEncoder
					.encode(parentid.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 根据群分类获取群级别列表
	 * 
	 * @param cateid
	 * @return
	 */
	public String getLevelsByCateId(Integer cateid) {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cateId", URLEncoder.encode(cateid.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 删除群
	 * 
	 * @return
	 */
	public Integer del(Integer groupid) throws GroupException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("groupId", URLEncoder
					.encode(groupid.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			int resultback = Integer.valueOf(rs);
			switch (resultback) {
			case -2:
				throw new GroupException(resultback, "要删除的群不存在");
			case -1:
				throw new GroupException(resultback, "删除群时意外异常");
			case 1:
				return 1;
			default:
				throw new GroupException(resultback, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
	}

	/**
	 * 更新群信息
	 * 
	 * @param gu
	 * @return
	 * @throws GroupUpdateException
	 */

	public Integer update(GroupUpdate gu) throws GroupUpdateException {
		try {
			JSONObject json = JSONObject.fromObject(gu);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}
			switch (status) {
			case 0:
				throw new GroupUpdateException(status, "数据提交出现异常");
			case -10:
				throw new GroupUpdateException(status, "没有找到要更新的群");
			default:
				throw new GroupUpdateException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupUpdateException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new GroupUpdateException("提交参数有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 创建一个群
	 * 
	 * @return
	 */
	public Integer create(GroupCreate gc) throws GroupCreateException {
		try {
			JSONObject json = JSONObject.fromObject(gc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer status = Integer.valueOf(rs);
			if (status > 0) {
				return status;
			}
			switch (status) {
			case 0:
				throw new GroupCreateException(status, "数据提交出现异常");
			default:
				throw new GroupCreateException(status, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupCreateException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new GroupCreateException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	public String create1(GroupCreate gc) {
		try {
			JSONObject json = JSONObject.fromObject(gc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			JSONObject r = JSONObject.fromObject(rs);
			if (r.has("success") && !r.getBoolean("success")) {
				int status = r.getInt("error");
				switch (status) {
				case 0:
					throw new GroupCreateException(status, "数据提交出现异常");
				default:
					throw new GroupCreateException(status, OTHER_ERROR);
				}
			} else {
				return rs;
			}
		} catch (NumberFormatException e) {
			throw new GroupCreateException("没有返回预期的数据!", e);
		} catch (UnsupportedEncodingException e) {
			throw new GroupCreateException("参数传递有误!", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		}
	}

	/**
	 * 从指定群中删除指定用户
	 * 
	 * @return
	 */
	public Integer delUsers(GroupUserDelete gud) throws GroupException {
		try {
			JSONObject json = JSONObject.fromObject(gud);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String rString = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rString);
			}
			Integer resutlInteger = Integer.valueOf(rString);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1.没有指定要删除的群,\n\t2.没有指定要删除的用户");
			case -1:
				throw new GroupException(resutlInteger, "指定删除的组不存在");
			case -2:
				throw new GroupException(resutlInteger, "指定删除的用户不存在");
			default:
				throw new GroupException(resutlInteger, OTHER_ERROR);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
	}

	/**
	 * 获得组用户
	 * 
	 * @param groupid
	 * @return
	 * @throws GroupException
	 */
	public String getUsers(Integer groupid) throws GroupException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("groupId", URLEncoder
					.encode(groupid.toString(), "UTF-8"));
			String rs = postReturnString(params);
			return rs;
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
	}

	/**
	 * 给指定群添加指定的用户
	 * 
	 * @param gua
	 * @return
	 * @throws GroupUserAddException
	 */
	public Integer addUsers(GroupUserAdd gua) throws GroupUserAddException {
		try {
			JSONObject json = JSONObject.fromObject(gua);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupUserAddException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1.用户没有指定要添加到哪个群,\n\t2.没有指定待添加的用户");
			case -1:
				throw new GroupUserAddException(resutlInteger, "指定的群不存在");
			case -2:
				throw new GroupUserAddException(resutlInteger, "指定添加的用户不存在");
			default:
				throw new GroupUserAddException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupUserAddException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupUserAddException(e.getMessage(), e);
		}
	}

	public Integer groupSortByType(GroupSort gs) {
		try {
			JSONObject json = JSONObject.fromObject(gs);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应群");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public Integer groupSortByMarketOrProvinceSort(GroupSort gs) {
		try {
			JSONObject json = JSONObject.fromObject(gs);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应群");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public String getGroupByParentTypes(Groups group) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(group);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}
}
