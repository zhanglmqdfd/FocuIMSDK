package cn.com.focu.im.sdk.service.addrbook.entity;

import cn.com.focu.im.sdk.service.addrbook.exception.ArrowException;

public class ArrowUser extends Arrow {

	private String loginNumber;

	private String tloginNumber;

	public ArrowUser(String direction, String loginNumber) {
		super(direction);
		if (!direction.equals("T") && !direction.equals("B"))
			throw new ArrowException(-1, "该构造只允许移动到[T|B]");
		this.loginNumber = loginNumber;
	}

	public ArrowUser(String direction, String loginNumber, String tloginNumber) {
		super(direction);
		if (!direction.equals("U") && !direction.equals("D"))
			throw new ArrowException(-1, "该构造只允许移动到[U|D]");
		this.loginNumber = loginNumber;
		this.tloginNumber = tloginNumber;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public String getTloginNumber() {
		return tloginNumber;
	}
}
