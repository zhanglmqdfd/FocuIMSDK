package cn.com.focu.im.sdk.service.user.entity;

public class UserMobileExists {
	private Integer uid;
	private String mobile;
	
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public UserMobileExists(Integer uid, String mobile) {
		super();
		this.uid = uid;
		this.mobile = mobile;
	}
	public UserMobileExists(String mobile) {
		super();
		this.mobile = mobile;
	}
}
