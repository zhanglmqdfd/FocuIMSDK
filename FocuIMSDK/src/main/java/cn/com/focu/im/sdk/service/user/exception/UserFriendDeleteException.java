package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserFriendDeleteException extends ServiceException {

	private static final long serialVersionUID = 1665732404611800615L;

	public UserFriendDeleteException(int code, String message) {
		super(code, message);
	}

	public UserFriendDeleteException(String message, Throwable cause) {
		super(message, cause);
	}
}
