package cn.com.focu.im.sdk.service.addrbook.exception;

public class ArrowUserException extends ArrowException{

	private static final long serialVersionUID = 4747545714365406201L;

	public ArrowUserException(int code, String message) {
		super(code, message);
	}

	public ArrowUserException(String message, Throwable cause) {
		super(message, cause);
	}
}
