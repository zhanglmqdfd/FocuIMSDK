package cn.com.focu.im.sdk.service.user.exception;

import cn.com.focu.im.sdk.service.exception.ServiceException;

public class UserPasswdException extends ServiceException{

	private static final long serialVersionUID = 180500354938571501L;

	public UserPasswdException(int code, String message) {
		super(code, message);
	}

	public UserPasswdException(String message, Throwable cause) {
		super(message, cause);
	}
}
