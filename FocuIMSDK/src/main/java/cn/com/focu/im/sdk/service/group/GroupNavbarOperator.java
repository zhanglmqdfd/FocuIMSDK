package cn.com.focu.im.sdk.service.group;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;
import cn.com.focu.im.sdk.service.ServiceOperator;
import cn.com.focu.im.sdk.service.annotation.Operator;
import cn.com.focu.im.sdk.service.csm.exception.CorpCustomerException;
import cn.com.focu.im.sdk.service.exception.ServiceException;
import cn.com.focu.im.sdk.service.group.entity.GroupCollect;
import cn.com.focu.im.sdk.service.group.entity.GroupCurrentSizeUpdate;
import cn.com.focu.im.sdk.service.group.entity.GroupHotCreator;
import cn.com.focu.im.sdk.service.group.entity.GroupHotCreatorSort;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbar;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarCurrentSizeUpdate;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarRelations;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarRelationsSort;
import cn.com.focu.im.sdk.service.group.entity.GroupNavbarSort;
import cn.com.focu.im.sdk.service.group.exception.GroupException;
import cn.com.focu.im.sdk.service.group.exception.GroupNavbarException;

/**
 * 
 * @author Administrator
 * 
 */
@Operator("groupNavbar")
public class GroupNavbarOperator extends ServiceOperator {

	private GroupNavbarOperator() {

	}

	public static class GroupNavbarOperatorHolder {
		static GroupNavbarOperator go = new GroupNavbarOperator();
	}

	public static GroupNavbarOperator getInstence() {
		return GroupNavbarOperatorHolder.go;
	}

	/**
	 * 增加群导航栏
	 * 
	 * @param gn
	 * @return
	 */
	public Integer addGroupNavbar(GroupNavbar gn) {
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1.输入的导航栏名称不能为空");
			case -1:
				throw new GroupNavbarException(resutlInteger, "提交数据格式错误");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 删除群导航栏
	 * 
	 * @param gn
	 * @return
	 */
	public Integer delGroupNavbar(GroupNavbar gn) {
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1导航栏id为空");
			case -1:
				throw new GroupNavbarException(resutlInteger, "提交数据格式错误");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 群导航栏排序
	 * 
	 * @param gns
	 * @return
	 */
	public Integer groupNavbarSort(GroupNavbarSort gns) {
		try {
			JSONObject json = JSONObject.fromObject(gns);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的导航栏");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 得到所有群导航栏
	 * 
	 * @param gn
	 * @return
	 */
	public String getGroupNavbarReturnPage(GroupNavbar gn) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 添加群至群导航栏下
	 * 
	 * @param gnr
	 * @return
	 */
	public Integer addGroupToNavbar(GroupNavbarRelations gnr) {
		try {
			JSONObject json = JSONObject.fromObject(gnr);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群组");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 删除导航栏下的群组
	 * 
	 * @param gnr
	 * @return
	 */
	public Integer delGroupNavbarRelations(GroupNavbarRelations gnr) {
		try {
			JSONObject json = JSONObject.fromObject(gnr);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群组");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 各个导航栏下的群组进行排序
	 * 
	 * @param gnr
	 * @return
	 */
	public Integer GroupNavbarRelationsSort(GroupNavbarRelationsSort gnrs) {
		try {
			JSONObject json = JSONObject.fromObject(gnrs);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群组");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 分页得到各导航栏下的所有群组
	 * 
	 * @param gnr
	 * @return
	 */
	public String getGroupNavbarRelationsReturnPage(GroupNavbarRelations gnr) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(gnr);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 修改每个群组下的在线人数和总人数
	 * 
	 * @param gcsu
	 * @return
	 */
	public int updateGroupCurrentSize(GroupCurrentSizeUpdate gcsu) {
		try {
			JSONObject json = JSONObject.fromObject(gcsu);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群组");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}
	
	public int updateNavbarCurrentSize(GroupNavbarCurrentSizeUpdate gncsu) {
		try {
			JSONObject json = JSONObject.fromObject(gncsu);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群组");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 根据群聊天数量得到最热门群组
	 * 
	 * @return
	 */
	public Integer addHotGroupToNavbar(Integer navbarId) {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("navbarId", URLEncoder.encode(navbarId.toString(),
					"UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer resutlInteger = Integer.valueOf(rs);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	/**
	 * 得到所有群主
	 * 
	 * @return
	 */
	public String getGroupCreator(GroupHotCreator ghc) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(ghc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 得到所有热门群主
	 * 
	 * @param ghc
	 * @return
	 */
	public String getHotGroupCreator(GroupHotCreator ghc) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(ghc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}

	/**
	 * 增加热门群主
	 * 
	 * @param ghc
	 * @return
	 */
	public Integer addGroupHotCreator(GroupHotCreator ghc) {
		try {
			JSONObject json = JSONObject.fromObject(ghc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public Integer delGroupHotCreator(GroupHotCreator ghc) {
		try {
			JSONObject json = JSONObject.fromObject(ghc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的热门群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public Integer groupHotCreatorSort(GroupHotCreatorSort ghcs) {
		try {
			JSONObject json = JSONObject.fromObject(ghcs);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的热门群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public Integer modifNavbarName(GroupNavbar gn) {
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的热门群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public Integer autoAddHotGroupCreator() {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer resutlInteger = Integer.valueOf(rs);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public Integer autoAddHotTalkGroup(Integer navbarId) {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("navbarId", URLEncoder.encode(navbarId.toString(),
					"UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
			Integer resutlInteger = Integer.valueOf(rs);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}

	public String getGroupNavbarById(GroupNavbar gn) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}
	
	public Integer getGroupCurrentSizeSum(GroupNavbarRelations gnr) {
		try {
			JSONObject json = JSONObject.fromObject(gnr);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的热门群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}
	
	public String getAutoGroup(GroupNavbar gn) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}
	
	public String getAutoHotGroupCreator() throws GroupException {
		String rs = "";
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}
	
	public String getAutoHotTalkGroup(GroupNavbar gn) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(gn);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupException(e.getMessage(), e);
		}
		return rs;
	}
	
	/**
	 * 
	 * @param gc
	 * @return
	 */
	public String groupCollectList(GroupCollect gc) {
		String rs = "";
		try {
			JSONObject json = JSONObject.fromObject(gc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			rs = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(rs);
			}

		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new CorpCustomerException(e.getMessage(), e);
		}
		return rs;
	}
	
	public Integer collectGroup(GroupCollect gc) {
		try {
			JSONObject json = JSONObject.fromObject(gc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的热门群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}
	
	public Integer cancelCollectGroup(GroupCollect gc) {
		try {
			JSONObject json = JSONObject.fromObject(gc);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("jsonString", URLEncoder
					.encode(json.toString(), "UTF-8"));
			String re = postReturnString(params);
			if (log.isDebugEnabled()) {
				log.debug(re);
			}
			Integer resutlInteger = Integer.valueOf(re);
			if (resutlInteger > 0) {
				return resutlInteger;
			}
			switch (resutlInteger) {
			case 0:
				throw new GroupNavbarException(resutlInteger,
						"数据提交失败，原因可能有：\n\t1数据格式错误");
			case -1:
				throw new GroupNavbarException(resutlInteger, "没有找到对应的热门群主");
			default:
				throw new GroupNavbarException(resutlInteger, OTHER_ERROR);
			}
		} catch (NumberFormatException e) {
			throw new GroupNavbarException("返回数值预期格式异常", e);
		} catch (IOException e) {
			throw new ServiceException(CANNOT_CONNECT_TO_SERVER);
		} catch (Exception e) {
			throw new GroupNavbarException(e.getMessage(), e);
		}
	}
}
