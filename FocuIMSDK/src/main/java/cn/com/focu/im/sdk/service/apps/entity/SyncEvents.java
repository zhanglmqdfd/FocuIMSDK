package cn.com.focu.im.sdk.service.apps.entity;

public class SyncEvents {

	private String loginName;
	private Integer event;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getEvent() {
		return event;
	}

	public void setEvent(Integer event) {
		this.event = event;
	}
}
