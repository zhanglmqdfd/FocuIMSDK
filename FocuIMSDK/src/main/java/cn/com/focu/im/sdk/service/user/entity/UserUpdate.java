package cn.com.focu.im.sdk.service.user.entity;

public class UserUpdate {
	
	/**
	 * 帐号标识,0:编号,1:别名,2:IM帐号
	 */
	private Integer flag = 0;
	
	/**
	 * 用户编号
	 */
	private Integer uid;
	
	/**
	 * IM帐号
	 */
	private String loginNumber;

	/**
	 * IM别名
	 */
	private String loginName;

	/**
	 * 邮箱
	 */
	private String email;
	
	/**
	 * 允许邮箱登陆
	 */
	private boolean allowEmailLogin;
	
	/**
	 * 手机号
	 */
	private String mobile;
	
	/**
	 * 允许手机号登陆
	 */
	private boolean allowMobileLogin;
	
	/**
	 * 昵称
	 */
	private String displayName;

	/**
	 * 年龄
	 */
	private Integer age = 18;

	/**
	 * 生日
	 */
	private String birthday = "2011-01-01";

	/**
	 * 性别
	 */
	private Integer sex = 1;

	/**
	 * 等级
	 */
	private Integer grade = 0;

	/**
	 * 部门编号
	 */
	private Integer branchId;
	
	private String englishName;
	
	private boolean englishNameChanged = false;
	
	private String watchword;
	
	private boolean watchwordChanged = false;
	
	private String headFileName;
	
	private boolean headFileNameChanged = false;
	
	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
		this.englishNameChanged = true;
	}
	
	public String getWatchword() {
		return watchword;
	}

	public void setWatchword(String watchword) {
		this.watchword = watchword;
		this.watchwordChanged = true;
	}

	public boolean isWatchwordChanged() {
		return watchwordChanged;
	}

	public boolean isEnglishNameChanged() {
		return englishNameChanged;
	}
	
	public String getHeadFileName() {
		return headFileName;
	}

	public void setHeadFileName(String headFileName) {
		this.headFileName = headFileName;
		this.headFileNameChanged = true;
	}

	public boolean isHeadFileNameChanged() {
		return headFileNameChanged;
	}

	public String getLoginNumber() {
		return loginNumber;
	}

	public void setLoginNumber(String loginNumber) {
		this.flag = 2;
		this.loginNumber = loginNumber;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.flag = 0;
		this.loginName = loginName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public boolean isAllowEmailLogin() {
		return allowEmailLogin;
	}

	public void setAllowEmailLogin(boolean allowEmailLogin) {
		this.allowEmailLogin = allowEmailLogin;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public boolean isAllowMobileLogin() {
		return allowMobileLogin;
	}

	public void setAllowMobileLogin(boolean allowMobileLogin) {
		this.allowMobileLogin = allowMobileLogin;
	}

	public Integer getFlag() {
		return flag;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.flag = 1;
		this.uid = uid;
	}
}
