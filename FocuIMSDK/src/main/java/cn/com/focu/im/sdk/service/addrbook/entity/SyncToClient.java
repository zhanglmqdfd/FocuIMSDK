package cn.com.focu.im.sdk.service.addrbook.entity;

public class SyncToClient {

	private String corpCode;
	private Integer corpId;
	private Integer flag = 0;

	public SyncToClient(String corpCode) {
		super();
		this.corpCode = corpCode;
		this.flag = 1;
	}

	public SyncToClient(Integer corpId) {
		super();
		this.corpId = corpId;
		this.flag = 0;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public Integer getCorpId() {
		return corpId;
	}

	public Integer getFlag() {
		return flag;
	}
}
